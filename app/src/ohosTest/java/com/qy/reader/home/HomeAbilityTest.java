/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qy.reader.home;

import com.qy.reader.EventHelper;
import com.qy.reader.ResourceTable;
import com.qy.reader.book.BookInfoAbility;
import com.qy.reader.book.read.ReadAbility;
import com.qy.reader.common.widgets.reader.ReadView;
import com.qy.reader.search.SearchAbility;
import com.qy.reader.search.result.SearchResultAbility;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.aafwk.ability.delegation.IAbilityDelegator;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ListContainer;
import ohos.agp.components.TextField;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.miscservices.timeutility.Time;
import ohos.multimodalinput.event.TouchEvent;
import ohos.utils.net.Uri;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class HomeAbilityTest {

    public HomeAbilityTest() {
    }

    private static IAbilityDelegator sAbilityDelegator = AbilityDelegatorRegistry.getAbilityDelegator();
    private Ability topAbility;
    private HomeAbility homeAbility;
    private SearchAbility searchAbility;
    private SearchResultAbility searchResultAbility;
    private BookInfoAbility bookInfoAbility;
    private ReadAbility readAbility;

    @Before
    public void setUp() throws Exception {
        homeAbility = EventHelper.startAbility(HomeAbility.class, getAbilityIntent("qyreader://home"));
        EventHelper.waitForActive(homeAbility, 5);
        Thread.sleep(2000L);
    }

    @After
    public void tearDown() throws Exception {
        Thread.sleep(500L);
        sAbilityDelegator.stopAbility(homeAbility);
        EventHelper.clearAbilities();
    }

    private Intent getAbilityIntent(String uri) {
        Intent intent = new Intent();
        Intent.OperationBuilder builder = new Intent.OperationBuilder()
                .withDeviceId("")
                .withBundleName(sAbilityDelegator.getAppContext().getBundleName());
        builder.withUri(Uri.parse(uri));
        intent.setOperation(builder.build());
        return intent;
    }

    private void simulateClickOnScreen(Ability ability, Component component) {
        int[] pos = component.getLocationOnScreen();
        pos[0] += component.getWidth() / 2;
        pos[1] += component.getHeight() / 2;

        simulateClickOnScreen(ability, pos[0], pos[1]);
    }

    private void simulateClickOnScreen(Ability ability, int x, int y) {
        long downTime = Time.getRealActiveTime();
        long upTime = downTime;
        TouchEvent ev1 = EventHelper.obtainTouchEvent(downTime, downTime, EventHelper.ACTION_DOWN, x, y);
        sAbilityDelegator.triggerTouchEvent(ability, ev1);
        TouchEvent ev2 = EventHelper.obtainTouchEvent(downTime, upTime, EventHelper.ACTION_UP, x, y);
        sAbilityDelegator.triggerTouchEvent(ability, ev2);
    }

    @Test
    public void testSearch() throws Exception {
        homeAbility.findComponentById(com.qy.reader.common.ResourceTable.Id_tv_tab_search).callOnClick();
        Thread.sleep(500L);

        topAbility = sAbilityDelegator.getCurrentTopAbility();
        assertTrue(topAbility instanceof SearchAbility);
        searchAbility = (SearchAbility) topAbility;

        new EventHandler(EventRunner.getMainEventRunner()).postTask(() -> {
            TextField textField = (TextField) searchAbility.findComponentById(com.qy.reader.common.ResourceTable.Id_et_search_bar_view);
            textField.setText("a");
        });
        Thread.sleep(500);
        searchAbility.findComponentById(com.qy.reader.common.ResourceTable.Id_tv_search_bar_search).callOnClick();
        Thread.sleep(5000L);

        topAbility = sAbilityDelegator.getCurrentTopAbility();
        assertTrue(topAbility instanceof SearchResultAbility);
        searchResultAbility = (SearchResultAbility) topAbility;

        ListContainer listContainer = (ListContainer) searchResultAbility.findComponentById(ResourceTable.Id_rv_search_list);
        // assert search result list not empty
        if (listContainer.getItemProvider().getCount() == 0) {
            // try again
            searchResultAbility.terminateAbility();
            Thread.sleep(500);

            searchAbility.findComponentById(com.qy.reader.common.ResourceTable.Id_tv_search_bar_search).callOnClick();
            Thread.sleep(5000L);

            topAbility = sAbilityDelegator.getCurrentTopAbility();
            assertTrue(topAbility instanceof SearchResultAbility);
            searchResultAbility = (SearchResultAbility) topAbility;

            listContainer = (ListContainer) searchResultAbility.findComponentById(ResourceTable.Id_rv_search_list);
        }
        assertTrue(listContainer.getItemProvider().getCount() > 0);

    }

    @Test
    public void testChapterList() throws Exception {
        testSearch();

        ListContainer bookListContainer = (ListContainer) searchResultAbility.findComponentById(ResourceTable.Id_rv_search_list);
        simulateClickOnScreen(searchResultAbility, bookListContainer.getComponentAt(1));
        Thread.sleep(5000L);

        topAbility = sAbilityDelegator.getCurrentTopAbility();
        assertTrue(topAbility instanceof BookInfoAbility);
        bookInfoAbility = (BookInfoAbility) topAbility;

        ListContainer chapterListContainer = (ListContainer) bookInfoAbility.findComponentById(ResourceTable.Id_rv_search_list);
        // assert chapter list not empty
        assertTrue(chapterListContainer.getItemProvider().getCount() > 0);
    }

    @Test
    public void testChapterContent() throws Exception {
        testChapterList();

        ListContainer chapterListContainer = (ListContainer) bookInfoAbility.findComponentById(ResourceTable.Id_rv_search_list);
        simulateClickOnScreen(bookInfoAbility, chapterListContainer.getComponentAt(0));
        Thread.sleep(5000);

        topAbility = sAbilityDelegator.getCurrentTopAbility();
        assertTrue(topAbility instanceof ReadAbility);
        readAbility = (ReadAbility) topAbility;

        ReadView readView = (ReadView) readAbility.findComponentById(ResourceTable.Id_read_view);
        int bufferLen = readView.getPageFactory().getBufferLen();
        assertTrue(bufferLen > 0);
    }
}