/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qy.reader.crawler;

import com.qy.reader.common.entity.book.SearchBook;
import com.qy.reader.common.entity.chapter.Chapter;
import com.qy.reader.crawler.source.SourceManager;
import com.qy.reader.crawler.source.callback.ChapterCallback;
import com.qy.reader.crawler.source.callback.ContentCallback;
import com.qy.reader.crawler.source.callback.SearchCallback;
import junit.framework.TestCase;

import java.util.List;

public class CrawlerTest extends TestCase {

    private String keyword = null;
    private List<SearchBook> appendList = null;
    private List<Chapter> chapterList = null;
    private String content = null;

    public void testSearch() throws InterruptedException {
        Crawler.search("a", new SearchCallback() {
            @Override
            public void onResponse(String keyword, List<SearchBook> appendList) {
                CrawlerTest.this.keyword = keyword;
                CrawlerTest.this.appendList = appendList;
            }

            @Override
            public void onFinish() {

            }

            @Override
            public void onError(String msg) {

            }
        });
        Thread.sleep(10000L);
        assertEquals("a", keyword);
        assertNotNull(appendList);
        assertTrue(appendList.size() > 0);
        assertTrue(appendList.get(0).title.contains("a") || appendList.get(0).author.contains("a") || appendList.get(0).desc.contains("a"));
    }

    public void testCatalog() throws InterruptedException {
        Crawler.catalog(new SearchBook.SL("https://www.81book.com/book/62867/", SourceManager.SOURCES.get(2).get()), new ChapterCallback() {
            @Override
            public void onResponse(List<Chapter> chapters) {
                CrawlerTest.this.chapterList = chapters;
            }

            @Override
            public void onError(String msg) {
            }
        });
        Thread.sleep(10000L);
        assertNotNull(chapterList);
        assertTrue(chapterList.size() > 0);
    }

    public void testContent() throws InterruptedException {
        Crawler.content(new SearchBook.SL("https://www.81book.com/book/62867/", SourceManager.SOURCES.get(2).get()), "/book/62867/29013513.html", new ContentCallback() {
            @Override
            public void onResponse(String content) {
                CrawlerTest.this.content = content;
            }

            @Override
            public void onError(String msg) {
            }
        });
        Thread.sleep(10000L);
        assertNotNull(content);
        assertTrue(content.length() > 0);
    }
}