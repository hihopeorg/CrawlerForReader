/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qy.reader.crawler.xpath.util;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class OpEmTest {

    @Test
    public void testVal() {
        assertEquals("+", OpEm.PLUS.val());
        assertEquals("-", OpEm.MINUS.val());
        assertEquals("=", OpEm.EQ.val());
        assertEquals("!=", OpEm.NE.val());
        assertEquals(">", OpEm.GT.val());
        assertEquals("<", OpEm.LT.val());
        assertEquals(">=", OpEm.GE.val());
        assertEquals("<=", OpEm.LE.val());
        assertEquals("^=", OpEm.STARTWITH.val());
        assertEquals("$=", OpEm.ENDWITH.val());
        assertEquals("*=", OpEm.CONTAIN.val());
        assertEquals("~=", OpEm.REGEX.val());
        assertEquals("!~", OpEm.NOTMATCH.val());
    }

    @Test
    public void testExecute() {
        assertEquals(5, OpEm.PLUS.execute("2", "3"));
        assertEquals(-1, OpEm.MINUS.execute("2", "3"));
        assertEquals(false, OpEm.EQ.execute("2", "3"));
        assertEquals(true, OpEm.NE.execute("2", "3"));
        assertEquals(false, OpEm.GT.execute("2", "3"));
        assertEquals(true, OpEm.LT.execute("2", "3"));
        assertEquals(false, OpEm.GE.execute("2", "3"));
        assertEquals(true, OpEm.LE.execute("2", "3"));
        assertEquals(true, OpEm.STARTWITH.execute("hello", "he"));
        assertEquals(true, OpEm.ENDWITH.execute("hello", "lo"));
        assertEquals(true, OpEm.CONTAIN.execute("hello", "ll"));
        assertEquals(true, OpEm.REGEX.execute("hello world", ".*lo.w.*"));
        assertEquals(true, OpEm.NOTMATCH.execute("hello world", ".*ho[a-z]w.*"));
    }
}