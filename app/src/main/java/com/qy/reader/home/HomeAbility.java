package com.qy.reader.home;

import com.qy.reader.common.base.BaseTabAbility;
import ohos.aafwk.ability.fraction.Fraction;

/**
 * Created by yuyuhang on 2018/1/9.
 */
public class HomeAbility extends BaseTabAbility {

    @Override
    protected int getCurrentIndex() {
        return HOME_INDEX;
    }

    @Override
    protected Fraction fractionInstance() {
        return new HomeFraction();
    }
}
