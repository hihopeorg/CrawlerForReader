package com.qy.reader.home;

import com.qy.reader.ResourceTable;
import com.qy.reader.common.base.BaseAbility;
import com.qy.reader.common.utils.Nav;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

/**
 * Created by yuyuhang on 2018/1/8.
 */
public class SplashAbility extends BaseAbility {

    private Text mTvSkip;


    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);

//        StatusBarCompat.compatTransNavigationBar(this);

        setUIContent(ResourceTable.Layout_ability_splash);

        mTvSkip = (Text) findComponentById(ResourceTable.Id_tv_splash_skip);
        mTvSkip.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component view) {
                end();
            }
        });

        handler.postTask(runnable, 500);
    }

    private EventHandler handler = new EventHandler(EventRunner.getMainEventRunner());

    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            end();
        }
    };

    private void end() {
        Nav.from(this).start("qyreader://home");

        terminateAbility();
    }

    @Override
    protected void onStop() {
        handler.removeTask(runnable);
        super.onStop();
    }
}
