package com.qy.reader.home;

import com.qy.reader.ResourceTable;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

/**
 * Created by yuyuhang on 2018/1/9.
 */
public class HomeFraction extends Fraction {

    private Component mRootView;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
    }


    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        mRootView = scatter.parse(ResourceTable.Layout_fraction_main_home, container, false);

        return mRootView;
    }
}
