package com.qy.reader.widgets;

import com.qy.reader.common.ResourceTable;
import com.qy.reader.common.utils.Nav;
import com.qy.reader.search.source.SourceSettingAbility;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.multimodalinput.event.KeyEvent;
import ohos.utils.PacMap;


/**
 * Created by xiaoshu on 2018/1/9.
 */
public class CustomSearchBar extends DirectionalLayout {

    private TextField mEtSearch = null;
    private Image mIvDelete = null;
    private Text mTvSearch = null;
    private Image mIvSource = null;

    public CustomSearchBar(Context context) {
        super(context);
        initView(context);
    }

    public CustomSearchBar(Context context, AttrSet attrSet) {
        super(context, attrSet);
        initView(context);
    }

    public CustomSearchBar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initView(context);
    }

    private void initView(Context context) {
        Component contentView = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_view_search_bar, this, true);
        mEtSearch = (TextField) contentView.findComponentById(ResourceTable.Id_et_search_bar_view);
        mIvDelete = (Image) contentView.findComponentById(ResourceTable.Id_iv_search_bar_delete);
        mTvSearch = (Text) contentView.findComponentById(ResourceTable.Id_tv_search_bar_search);
        mIvSource = (Image) contentView.findComponentById(ResourceTable.Id_iv_search_bar_source);
        setAlignment(LayoutAlignment.VERTICAL_CENTER);
        setOrientation(Component.HORIZONTAL);
        setLayoutConfig(new DirectionalLayout.LayoutConfig(DirectionalLayout.LayoutConfig.MATCH_PARENT, DirectionalLayout.LayoutConfig.MATCH_CONTENT));

        mEtSearch.setKeyEventListener(new KeyEventListener() {
            @Override
            public boolean onKeyEvent(Component component, KeyEvent keyEvent) {
                if (keyEvent.getKeyCode() == KeyEvent.KEY_ENTER && !keyEvent.isKeyDown()) {
                    search();
                    return true;
                }
                return false;
            }
        });

        mIvDelete.setClickedListener(component -> mEtSearch.setText(""));

        mTvSearch.setClickedListener(component -> search());

        mIvSource.setClickedListener(component -> {
            Intent intent = new Intent();
            Operation operation = new Intent.OperationBuilder()
                    .withDeviceId("")
                    .withBundleName(getContext().getBundleName())
                    .withAbilityName(SourceSettingAbility.class)
                    .build();
            intent.setOperation(operation);
            context.startAbility(intent, 0);
        });
    }

    private void search() {
        String text = mEtSearch.getText();
        if (text.isEmpty()) {
            new ToastDialog(getContext()).setText("请输入搜索内容").setDuration(200).show();
            return;
        }

        PacMap bundle = new PacMap();
        bundle.putString("text", text);
        Nav.from(getContext()).setExtras(bundle).start("qyreader://searchresult");
    }
}
