package com.qy.reader;

import ohos.aafwk.ability.AbilityPackage;

import com.qy.reader.common.Global;

/**
 * Created by yuyuhang on 2018/1/8.
 */
public class App extends AbilityPackage {

    @Override
    public void onInitialize() {
        super.onInitialize();

        Global.init(this);
    }
}
