package com.qy.reader.book;

import com.bumptech.glide.Glide;
import com.qy.reader.ResourceTable;
import com.qy.reader.common.base.BaseAbility;
import com.qy.reader.common.entity.book.SearchBook;
import com.qy.reader.common.entity.chapter.Chapter;
import com.qy.reader.common.utils.Nav;
import com.qy.reader.common.utils.StringUtils;
import com.qy.reader.common.widgets.ListDialog;
import com.qy.reader.common.widgets.Sneaker;
import com.qy.reader.crawler.Crawler;
import com.qy.reader.crawler.source.callback.ChapterCallback;
import com.yuyh.easyadapter.recyclerview.EasyRVAdapter;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayoutManager;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.components.Text;
import ohos.agp.window.dialog.CommonDialog;
import ohos.utils.PacMap;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by quezhongsang on 2018/1/13.
 */

public class BookInfoAbility extends BaseAbility {

    private ListContainer mRecyclerView;
    private BookInfoAdapter mBookInfoAdapter;
    private ArrayList<Chapter> mChapterList = new ArrayList<>();

    private List<SearchBook.SL> mSourceList = new ArrayList<>();

    private Text mTvBookSource;
    private Text mTvBookNewestChapter;
    private Text mTvBookOrderBy;

    private String mStrDesc = "倒序";
    private String mStrAsc = "正序";

    private SearchBook mSearchBook;

    private int mCurrentSourcePosition = 0;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        Component root = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_ability_book_info, null, false);
        setUIContent((ComponentContainer)root);
        initToolbar();

        mSearchBook = (SearchBook) getIntent().getSerializableParam("search_book");
        if (mSearchBook == null) {
            Sneaker.with(mContext)
                    .setTitle("加载失败")
                    .setMessage("search_book不能为空")
                    .sneakWarning();
            return;
        }

        initView();
    }

    @Override
    public String getToolbarTitle() {
        return "书籍详情";
    }

    private void initView() {
        if (mSearchBook.sources != null) {
            mSourceList.addAll(mSearchBook.sources);
        }

        mRecyclerView = (ListContainer) findComponentById(ResourceTable.Id_rv_search_list);
        //book
        Glide.with(this).load(mSearchBook.cover).into((Image) findComponentById(ResourceTable.Id_iv_book_cover));
        ((Text) findComponentById(ResourceTable.Id_tv_book_title)).setText(StringUtils.getStr(mSearchBook.title));
        ((Text) findComponentById(ResourceTable.Id_tv_book_author)).setText(StringUtils.getStr(mSearchBook.author));
        ((Text) findComponentById(ResourceTable.Id_tv_book_desc)).setText(StringUtils.getStr(mSearchBook.desc));

        mTvBookNewestChapter = (Text) findComponentById(ResourceTable.Id_tv_book_newest_chapter);
        mTvBookSource = (Text) findComponentById(ResourceTable.Id_tv_book_source);
        mTvBookOrderBy = (Text) findComponentById(ResourceTable.Id_tv_order_by);

        //list
        mRecyclerView.setLayoutManager(new DirectionalLayoutManager());
        mBookInfoAdapter = new BookInfoAdapter(mContext, mChapterList);
        mRecyclerView.setItemProvider(mBookInfoAdapter);
        mBookInfoAdapter.setOnItemClickListener(new EasyRVAdapter.OnItemClickListener<Chapter>() {
            @Override
            public void onItemClick(Component view, int position, Chapter item) {
                PacMap bundle = new PacMap();
                bundle.putSerializableObject("book", mSearchBook);
                bundle.putSerializableObject("chapter_list", mChapterList);
                bundle.putSerializableObject("chapter", item);
                bundle.putSerializableObject("source", mSourceList.get(mCurrentSourcePosition));
                Nav.from(mContext).setExtras(bundle).start("qyreader://read");
            }
        });

        findComponentById(ResourceTable.Id_tv_book_source).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                if (mSourceList.size() == 0)
                    return;
                String[] items = new String[mSourceList.size()];
                for (int i = 0; i < items.length; i++) {
                    items[i] = mSourceList.get(i).source.name;
                }
                new ListDialog.Builder(mContext)
                        .setList(items, new ListDialog.OnItemClickListener() {
                            @Override
                            public void onItemClick(CommonDialog materialDialog, int position, String content) {
                                if (mCurrentSourcePosition != position) {
                                    mCurrentSourcePosition = position;
                                    requestNet();
                                }
                            }
                        })
                        .show();
            }
        });

        mTvBookOrderBy.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component v) {
                String str = mTvBookOrderBy.getText().toString();
                if (mStrAsc.equalsIgnoreCase(str)) {//倒序
                    mBookInfoAdapter.orderByDesc();
                    mTvBookOrderBy.setText(mStrDesc);
                } else if (mStrDesc.equalsIgnoreCase(str)) {//正序
                    mBookInfoAdapter.orderByAsc();
                    mTvBookOrderBy.setText(mStrAsc);
                }
            }
        });

        requestNet();
    }

    private void requestNet() {
        final SearchBook.SL sl = getSL();
        if (sl == null) {
            return;
        }
        mTvBookSource.setText(getSourceStr());

        mBookInfoAdapter.clear();
        mTvBookOrderBy.setText("加载中...");

        new Thread(() -> Crawler.catalog(sl, new ChapterCallback() {
            @Override
            public void onResponse(List<Chapter> chapters) {
                getUITaskDispatcher().asyncDispatch(()->{
                    if (chapters == null || chapters.isEmpty()) {
                        return;
                    }
                    mBookInfoAdapter.addAll(chapters);

                    //set  最新章节
                    Chapter lastChapter = chapters.get(chapters.size() - 1);
                    if (lastChapter != null) {
                        mTvBookNewestChapter.setText("最新章节：" + StringUtils.getStr(lastChapter.title));
                    }

                    //set 排序名字
                    if (mBookInfoAdapter.isAsc()) {
                        mTvBookOrderBy.setText(mStrAsc);
                    } else {
                        mTvBookOrderBy.setText(mStrDesc);
                    }
                });
            }

            @Override
            public void onError(String msg) {
                getUITaskDispatcher().asyncDispatch(()-> {
                    Sneaker.with(mContext)
                            .setTitle("加载失败")
                            .setMessage(msg)
                            .sneakWarning();

                    mTvBookOrderBy.setText("加载失败");
                });
            }
        })).start();
    }


    private String getSourceStr() {
        String sourceStr = "来源(" + mSearchBook.sources.size() + ")：";
        if (mCurrentSourcePosition < mSearchBook.sources.size()) {
            SearchBook.SL sl = mSearchBook.sources.get(mCurrentSourcePosition);
            if (sl != null && sl.source != null) {
                sourceStr += StringUtils.getStr(sl.source.name);
            }
        }

        return sourceStr;
    }

    private SearchBook.SL getSL() {
        if (mCurrentSourcePosition < mSearchBook.sources.size()) {
            return mSearchBook.sources.get(mCurrentSourcePosition);
        }
        return null;
    }
}
