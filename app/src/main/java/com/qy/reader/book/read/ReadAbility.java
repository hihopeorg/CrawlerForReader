package com.qy.reader.book.read;

import com.qy.reader.ResourceTable;
import com.qy.reader.common.base.BaseAbility;
import com.qy.reader.common.entity.book.SearchBook;
import com.qy.reader.common.entity.chapter.Chapter;
import com.qy.reader.common.widgets.Sneaker;
import com.qy.reader.common.widgets.reader.BookManager;
import com.qy.reader.common.widgets.reader.OnPageStateChangedListener;
import com.qy.reader.common.widgets.reader.ReadView;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.LayoutScatter;
import ohos.agp.utils.TextTool;
import ohos.bundle.AbilityInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yuyuhang on 2018/1/13.
 */
public class ReadAbility extends BaseAbility implements ReadContract.View {

    private ReadView mReadView;

    private DependentLayout mRlTopBar;
    private DirectionalLayout mLLBottomBar;

    private TargetAnimator showReadBarAnim, hideReadBarAnim;

    private List<Chapter> mChapterList = new ArrayList<>();
    private SearchBook.SL mSource;
    private SearchBook mBook;
    private String mBookNum;

    private int currentChapter = 1, currentPage = 1;

    private ReadPresenter mPresenter;

    @Override
    public boolean enableStatusBarCompat() {
        return false;
    }

    @Override
    protected void onOrientationChanged(AbilityInfo.DisplayOrientation displayOrientation) {
        super.onOrientationChanged(displayOrientation);
        // 切换横竖屏，部分对象需要重新初始化
        if (mReadView != null) {
            if (displayOrientation == AbilityInfo.DisplayOrientation.LANDSCAPE) { //横屏
                mReadView.initScreenSize();
            } else if (displayOrientation == AbilityInfo.DisplayOrientation.PORTRAIT) { //竖屏
                mReadView.initScreenSize();
            }
        }
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
//        StatusBarCompat.compat(this, getColor(ResourceTable.Color_colorReadBar));
        Component root = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_ability_read, null, false);
        setUIContent((ComponentContainer)root);

        initData();

        initView();

        initChapter();
    }

    private void initData() {
        mPresenter = new ReadPresenter(this);

        showReadBarAnim = new TargetAnimator();
        showReadBarAnim.setCurveType(Animator.CurveType.LINEAR);
        showReadBarAnim.setDelay(300);
        showReadBarAnim.setValueUpdateListener((anim, v) -> {
            Component target = ((TargetAnimator) anim).getTarget();
            int height = target.getHeight();
            target.setTranslationX(height * -1.5f * (1 - v));
        });

        hideReadBarAnim = new TargetAnimator();
        hideReadBarAnim.setCurveType(Animator.CurveType.LINEAR);
        hideReadBarAnim.setDelay(300);
        hideReadBarAnim.setValueUpdateListener((anim, v) -> {
            Component target = ((TargetAnimator) anim).getTarget();
            int height = target.getHeight();
            target.setTranslationX(height * -1.5f * v);
        });
    }

    private static class TargetAnimator extends AnimatorValue {
        private Component target = null;

        public TargetAnimator setTarget(Component target) {
            this.target = target;
            return this;
        }

        public Component getTarget() {
            return target;
        }
    }

    private void initView() {
        mReadView = (ReadView) findComponentById(ResourceTable.Id_read_view);
        mReadView.setOnPageStateChangedListener(new PagerListener());
        mRlTopBar = (DependentLayout) findComponentById(ResourceTable.Id_rl_book_read_top);
        mLLBottomBar = (DirectionalLayout) findComponentById(ResourceTable.Id_ll_book_read_bottom);

        DependentLayout.LayoutConfig params = (DependentLayout.LayoutConfig) mRlTopBar.getLayoutConfig();
        params.setMarginTop(0);
        mRlTopBar.setLayoutConfig(params);

        showStatusBar();
    }

    private void initChapter() {
        List<Chapter> list = getIntent().getSerializableParam("chapter_list");
        if (list == null) {
            Sneaker.with(this).setTitle("章节加载失败").setMessage("chapter_list不能为空").sneakWarning();
            return;
        }
        mChapterList.addAll(list);

        mBook = getIntent().getSerializableParam("book");
        mSource = getIntent().getSerializableParam("source");
        mBookNum = BookManager.getInstance().getBookNum(mBook.title, mBook.author);

        Chapter chapter = getIntent().getSerializableParam("chapter");

        int chapterIndex = 0;
        if (chapter != null) {
            for (int i = 0; i < mChapterList.size(); i++) {
                Chapter c = mChapterList.get(i);
                if (TextTool.isEqual(c.link, chapter.link)) {
                    chapterIndex = i;
                    break;
                }
            }
        }
        currentChapter = chapterIndex + 1;

        mPresenter.getChapterContent(mBookNum, mSource, mChapterList.get(chapterIndex));
    }

    @Override
    public void showContent(Chapter chapter, String content) {
        if (!mReadView.isInit() && currentChapter == mChapterList.indexOf(chapter) + 1) {
            int pageIndex = 1;
            mReadView.initChapterList(mSource.source.id, mBookNum, mChapterList, currentChapter, pageIndex);
        }
        mReadView.invalidate();
    }

    private void hideReadBar() {
        hideStatusBar();
        gone(mRlTopBar, mLLBottomBar);
        hideReadBarAnim.setTarget(mRlTopBar).start();
        hideReadBarAnim.setTarget(mLLBottomBar).start();
//        getWindow().getDecorView().setSystemUiVisibility(Component.SYSTEM_UI_FLAG_LOW_PROFILE);
    }

    private void showReadBar() {
        showStatusBar();
        visible(mRlTopBar, mLLBottomBar);
        showReadBarAnim.setTarget(mRlTopBar).start();
        showReadBarAnim.setTarget(mLLBottomBar).start();
//        getWindow().getDecorView().setSystemUiVisibility(Component.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
    }

    private void toggleReadBar() {
        if (isVisible(mRlTopBar) || isVisible(mLLBottomBar)) {
            hideReadBar();
        } else {
            showReadBar();
        }
    }

    private class PagerListener implements OnPageStateChangedListener {

        @Override
        public void onCenterClick() {
            toggleReadBar();
        }

        @Override
        public void onChapterChanged(int currentChapter, int fromChapter, boolean fromUser) {
            ReadAbility.this.currentChapter = currentChapter;
            for (int i = currentChapter - 1; i <= mChapterList.size() && i <= currentChapter + 3; i++) {
                if (i > 0) {
                    mPresenter.getChapterContent(mBookNum, mSource, mChapterList.get(i - 1));
                }
            }
        }

        @Override
        public void onPageChanged(int currentPage, int currentChapter) {
            ReadAbility.this.currentPage = currentPage;
            if (isVisible(mRlTopBar)) {
                hideReadBar();
            }
        }

        @Override
        public void onChapterLoadFailure(int currentChapter) {
            for (int i = currentChapter - 1; i <= mChapterList.size() && i <= currentChapter + 3; i++) {
                if (i > 0) {
                    mPresenter.getChapterContent(mBookNum, mSource, mChapterList.get(i - 1));
                }
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.release();
    }
}
