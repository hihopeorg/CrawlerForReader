package com.qy.reader.mine;

import com.qy.reader.common.base.BaseTabAbility;
import ohos.aafwk.ability.fraction.Fraction;

/**
 * Created by yuyuhang on 2018/1/9.
 */
public class MineAbility extends BaseTabAbility {

    @Override
    protected int getCurrentIndex() {
        return MINE_INDEX;
    }

    @Override
    protected Fraction fractionInstance() {
        return new MineFraction();
    }
}
