package com.qy.reader;

import ohos.aafwk.content.Intent;
import ohos.aafwk.ability.Ability;
import ohos.agp.components.Text;

import com.qy.reader.common.entity.book.SearchBook;
import com.qy.reader.common.entity.chapter.Chapter;
import com.qy.reader.crawler.Crawler;
import com.qy.reader.crawler.source.SourceManager;
import com.qy.reader.crawler.source.callback.ChapterCallback;
import com.qy.reader.crawler.source.callback.ContentCallback;

import java.util.List;


public class MainAbility extends Ability {

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);

//        StatusBarCompat.compat(this);

        setUIContent(ResourceTable.Layout_ability_main);
        final Text textView = (Text) findComponentById(ResourceTable.Id_tv_Content);

        new Thread(new Runnable() {
            @Override
            public void run() {
//                Crawler.search("你好", new SearchCallback() {
//                    @Override
//                    public void onResponse(String keyword, List<SearchBook> appendList) {
//                        Log.e("key" + keyword, "" + appendList);
//                    }
//
//                    @Override
//                    public void onFinish() {
//
//                    }
//
//                    @Override
//                    public void onError(String msg) {
//
//                    }
//                });
                Crawler.catalog(new SearchBook.SL("https://www.liewen.cc/b/24/24934/", SourceManager.SOURCES.get(1).get()), new ChapterCallback() {
                    @Override
                    public void onResponse(List<Chapter> chapters) {

                    }

                    @Override
                    public void onError(String msg) {

                    }
                });

                Crawler.content(new SearchBook.SL("https://www.liewen.cc/b/24/24934/", SourceManager.SOURCES.get(1).get()), "/b/24/24934/12212511.html", new ContentCallback() {
                    @Override
                    public void onResponse(String content) {

                    }

                    @Override
                    public void onError(String msg) {

                    }
                });
            }
        }).start();


    }
}
