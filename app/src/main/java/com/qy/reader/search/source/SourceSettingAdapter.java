package com.qy.reader.search.source;

import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.StateElement;
import ohos.agp.render.Paint;
import ohos.app.Context;
import ohos.utils.PlainBooleanArray;
import ohos.agp.components.Checkbox;
import ohos.agp.components.AbsButton;

import com.qy.reader.ResourceTable;
import com.qy.reader.common.entity.source.Source;
import com.qy.reader.crawler.source.SourceManager;
import com.yuyh.easyadapter.recyclerview.EasyRVAdapter;
import com.yuyh.easyadapter.recyclerview.EasyRVHolder;

import java.util.List;

/**
 * Created by yuyuhang on 2018/1/12.
 */
public class SourceSettingAdapter extends EasyRVAdapter<Source> {

    private PlainBooleanArray checkedMap = new PlainBooleanArray();

    public SourceSettingAdapter(Context context, List<Source> list) {
        super(context, list, ResourceTable.Layout_item_search_source_setting);
        checkedMap = SourceManager.getSourceEnableSparseArray();
    }

    @Override
    protected void onBindData(EasyRVHolder viewHolder, int position, final Source item) {
        Checkbox box = viewHolder.getView(ResourceTable.Id_cb_item_source_setting);
        box.setCheckedStateChangedListener(new AbsButton.CheckedStateChangedListener() {
            @Override
            public void onCheckedChanged(AbsButton buttonView, boolean isChecked) {
                checkedMap.put(item.id, isChecked);
            }
        });
        box.setChecked(checkedMap.get(item.id).orElse(false));
        box.setText(item.name);

        StateElement element = (StateElement)box.getButtonElement();
        for(int i=0; i<element.getStateCount(); i++) {
            element.getStateElement(i).setBounds(0, 0, 24*3, 24*3);
        }
    }

    public PlainBooleanArray getCheckedMap() {
        return checkedMap;
    }

    @Override
    public Object getItem(int i) {
        return super.getData(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
}
