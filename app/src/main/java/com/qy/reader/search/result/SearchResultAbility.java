package com.qy.reader.search.result;

import com.qy.reader.ResourceTable;
import com.qy.reader.common.base.BaseAbility;
import com.qy.reader.common.entity.book.SearchBook;
import com.qy.reader.common.utils.Nav;
import com.qy.reader.common.widgets.Sneaker;
import com.qy.reader.crawler.Crawler;
import com.qy.reader.crawler.source.callback.SearchCallback;
import com.yuyh.easyadapter.recyclerview.EasyRVAdapter;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayoutManager;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.ListContainer;
import ohos.agp.utils.TextTool;
import ohos.utils.PacMap;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yuyuhang on 2018/1/9.
 */
public class SearchResultAbility extends BaseAbility {

    private ListContainer mRvSearchResult;
    private SearchResultAdapter mAdapter;

    private List<SearchBook> mList = new ArrayList<>();

    @Override
    public String getToolbarTitle() {
        return "搜索结果";
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        Component root = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_ability_search_result, null, false);
        setUIContent((ComponentContainer) root);
        initToolbar();

        String title = getIntent().getStringParam("text");
        if (TextTool.isNullOrEmpty(title)) {
            Sneaker.with(this)
                    .setTitle("搜索词不能为空哦！")
                    .sneakWarning();
            return;
        }

        mRvSearchResult = (ListContainer) findComponentById(ResourceTable.Id_rv_search_list);
        mRvSearchResult.setLayoutManager(new DirectionalLayoutManager());

        mAdapter = new SearchResultAdapter(this, mList);
        mRvSearchResult.setItemProvider(mAdapter);
        mAdapter.setOnItemClickListener(new EasyRVAdapter.OnItemClickListener<SearchBook>() {
            @Override
            public void onItemClick(Component view, int position, SearchBook item) {
                if (item == null)
                    return;

                PacMap bundle = new PacMap();
                bundle.putSerializableObject("search_book", item);
                Nav.from(SearchResultAbility.this).setExtras(bundle).start("qyreader://bookinfo");
            }
        });

        search(title);
    }

    private void search(final String title) {
        mAdapter.setTitle(title);

        new Thread() {
            @Override
            public void run() {
                Crawler.search(title, new SearchCallback() {
                @Override
                public void onResponse(String keyword, List<SearchBook> appendList) {
                    getUITaskDispatcher().asyncDispatch(()->{
                        if (appendList == null || appendList.isEmpty()) {
                            return;
                        }

                        for (SearchBook newBook : appendList) {
                            boolean exists = false;
                            for (SearchBook book : mList) {
                                if (TextTool.isEqual(book.title, newBook.title)
                                        && !newBook.sources.isEmpty()) {
                                    if (TextTool.isNullOrEmpty(book.cover) && !TextTool.isNullOrEmpty(newBook.cover)) {
                                        book.cover = newBook.cover;
                                    }
                                    book.sources.add(newBook.sources.get(0));
                                    exists = true;
                                    break;
                                }
                            }
                            if (!exists) {
                                mList.add(newBook);
                            }
                        }

                        mAdapter.notifyDataChanged();
                    });
                }

                @Override
                public void onFinish() {
                    getUITaskDispatcher().asyncDispatch(()->{
                        Sneaker.with(SearchResultAbility.this)
                                .setTitle("搜索完毕")
                                .setMessage("共搜索到" + mList.size() + "本书")
                                .sneakSuccess();
                    });
                }

                @Override
                public void onError(String msg) {
                    Sneaker.with(SearchResultAbility.this)
                            .setTitle("搜索失败")
                            .setMessage(msg)
                            .sneakWarning();
                }
            });
            }
        }.start();
    }
}
