package com.qy.reader.search;

import com.qy.reader.common.base.BaseTabAbility;
import ohos.aafwk.ability.fraction.Fraction;

/**
 * Created by yuyuhang on 2018/1/9.
 */
public class SearchAbility extends BaseTabAbility {

    @Override
    protected int getCurrentIndex() {
        return SEARCH_INDEX;
    }

    @Override
    protected Fraction fractionInstance() {
        return new SearchFraction();
    }
}
