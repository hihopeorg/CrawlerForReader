package com.qy.reader.search.result;

import ohos.agp.text.RichTextBuilder;
import ohos.agp.text.TextForm;
import ohos.app.Context;
import ohos.agp.utils.TextTool;
import ohos.agp.components.Image;
import ohos.agp.components.Text;

import com.bumptech.glide.Glide;
import com.qy.reader.ResourceTable;
import com.qy.reader.common.entity.book.SearchBook;
import com.qy.reader.common.entity.source.Source;
import com.qy.reader.crawler.source.SourceManager;
import com.yuyh.easyadapter.recyclerview.EasyRVAdapter;
import com.yuyh.easyadapter.recyclerview.EasyRVHolder;

import java.util.List;

/**
 * Created by yuyuhang on 2018/1/11.
 */
public class SearchResultAdapter extends EasyRVAdapter<SearchBook> {

    private String title;

    public SearchResultAdapter(Context context, List<SearchBook> list) {
        super(context, list, ResourceTable.Layout_item_search_list);
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    protected void onBindData(EasyRVHolder viewHolder, int position, SearchBook item) {

        Text tvTitle = viewHolder.getView(ResourceTable.Id_tv_search_item_title);
        highlightText(tvTitle, item.title, title, 0xFFF08080);

        Text tvAuthor = viewHolder.getView(ResourceTable.Id_tv_search_item_author);
        highlightText(tvAuthor, item.author, title, 0xFFF08080);

        Text tvDesc = viewHolder.getView(ResourceTable.Id_tv_search_item_desc);
        highlightText(tvDesc, item.desc, title, 0xFFF08080);

        Text tvSource = viewHolder.getView(ResourceTable.Id_tv_search_item_source);
        StringBuilder builder = new StringBuilder();
        for (SearchBook.SL sl : item.sources) {
            if (sl.source != null) {
                Source source = SourceManager.SOURCES.get(sl.source.id).get();
                if (source != null) {
                    if (builder.length() > 0) {
                        builder.append(" | ");
                    }
                    builder.append(source.name);
                }
            }
        }
        tvSource.setText(builder.toString());

        Image ivCover = viewHolder.getView(ResourceTable.Id_iv_search_item_cover);
        Glide.with(mContext).load(item.cover).into(ivCover);
    }

    private static void highlightText(final Text textView, final String originalText, String constraint, int color) {
        TextForm form = new TextForm();
        form.setTextFont(textView.getFont());
        form.setTextSize(textView.getTextSize());
        form.setTextColor(textView.getTextColor().getValue());
        RichTextBuilder builder = new RichTextBuilder(form);

        for (int i = 0; i < originalText.length(); ) {
            int pos = originalText.toLowerCase().indexOf(constraint.toLowerCase(), i);
            if (pos >= i) {
                if (pos > i) {
                    builder.addText(originalText.substring(i, pos));
                }
                TextForm newForm = new TextForm();
                newForm.setTextColor(color);
                builder.mergeForm(newForm).addText(originalText.substring(pos, pos+constraint.length())).revertForm();
                i = pos + constraint.length();
            } else {
                builder.addText(originalText.substring(i));
                break;
            }
        }

        textView.setRichText(builder.build());
    }

    @Override
    public Object getItem(int i) {
        return super.getData(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
}
