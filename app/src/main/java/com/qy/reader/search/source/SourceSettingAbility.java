package com.qy.reader.search.source;

import com.qy.reader.ResourceTable;
import com.qy.reader.common.base.BaseAbility;
import com.qy.reader.common.entity.source.Source;
import com.qy.reader.crawler.source.SourceManager;
import ohos.aafwk.content.Intent;
import ohos.agp.components.DirectionalLayoutManager;
import ohos.agp.components.ListContainer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yuyuhang on 2018/1/12.
 */
public class SourceSettingAbility extends BaseAbility {

    private ListContainer mRecyclerView;
    private List<Source> mList = new ArrayList<>();
    private SourceSettingAdapter mAdapter;

    @Override
    public String getToolbarTitle() {
        return "搜索源";
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        setUIContent(ResourceTable.Layout_ability_search_source_setting);
        initToolbar();

        mRecyclerView = (ListContainer) findComponentById(ResourceTable.Id_rv_source_setting_list);
        mRecyclerView.setLayoutManager(new DirectionalLayoutManager());
        for (int i = 0; i < SourceManager.SOURCES.size(); i++) {
            mList.add(SourceManager.SOURCES.valueAt(i));
        }
        mAdapter = new SourceSettingAdapter(this, mList);
        mRecyclerView.setItemProvider(mAdapter);
    }

    @Override
    protected void onInactive() {
        super.onInactive();

        SourceManager.saveSourceEnable(mAdapter.getCheckedMap());
    }
}
