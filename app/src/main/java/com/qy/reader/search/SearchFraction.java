package com.qy.reader.search;

import com.qy.reader.ResourceTable;
import com.qy.reader.widgets.CustomSearchBar;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;

/**
 * Created by xiaoshu on 2018/1/9.
 */
public class SearchFraction extends Fraction {

    private Component mToolBar;
    private CustomSearchBar mSearchBar;

    @Override
    protected Component onComponentAttached(LayoutScatter scatter, ComponentContainer container, Intent intent) {
        Component contentView = scatter.parse(ResourceTable.Layout_fraction_search, container, false);
        mToolBar = contentView.findComponentById(ResourceTable.Id_common_toolbar);
        mSearchBar = new CustomSearchBar(container.getContext());
        ((ComponentContainer) mToolBar).addComponent(mSearchBar);
        return contentView;
    }
}
