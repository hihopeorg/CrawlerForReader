package com.qy.reader.discover;

import com.qy.reader.common.base.BaseTabAbility;
import ohos.aafwk.ability.fraction.Fraction;

/**
 * Created by yuyuhang on 2018/1/9.
 */
public class DiscoverAbility extends BaseTabAbility {

    @Override
    protected int getCurrentIndex() {
        return DISCOVER_INDEX;
    }

    @Override
    protected Fraction fractionInstance() {
        return new DiscoverFraction();
    }
}
