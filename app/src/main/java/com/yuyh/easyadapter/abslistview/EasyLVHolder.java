package com.yuyh.easyadapter.abslistview;

import com.yuyh.easyadapter.helper.ViewHelper;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Checkbox;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Image;
import ohos.agp.components.LayoutScatter;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.utils.PlainArray;

import java.util.HashMap;

/**
 * @author yuyh.
 * @date 2016/7/21.
 */
public class EasyLVHolder implements ViewHelper.AbsListView<EasyLVHolder> {

    /**
     * findViewById后保存View集合
     */
    private PlainArray<Component> mViews = new PlainArray<>();
    private PlainArray<Component> mConvertViews = new PlainArray<>();

    private Component mConvertView;
    protected int mPosition;
    protected int mLayoutId;
    protected Context mContext;

    protected EasyLVHolder(Context context, int position, ComponentContainer parent, int layoutId) {
        this.mConvertView = mConvertViews.get(layoutId).orElse(null);
        this.mPosition = position;
        this.mContext = context;
        this.mLayoutId = layoutId;
        if (mConvertView == null) {
            mConvertView = LayoutScatter.getInstance(context).parse(layoutId, parent, false);
            mConvertViews.put(layoutId, mConvertView);
            mConvertView.setTag(this);
        }
    }

    protected EasyLVHolder() {
    }

    public <BVH extends EasyLVHolder> BVH get(Context context, int position, Component convertView, ComponentContainer parent, int layoutId) {
        if (convertView == null) {
            return (BVH) new EasyLVHolder(context, position, parent, layoutId);
        } else {
            EasyLVHolder holder = (EasyLVHolder) convertView.getTag();
            if (holder.mLayoutId != layoutId) {
                return (BVH) new EasyLVHolder(context, position, parent, layoutId);
            }
            holder.setPosition(position);
            return (BVH) holder;
        }
    }

    /**
     * 获取item布局
     * @return
     */
    public Component getConvertView() {
        return mConvertViews.valueAt(0);
    }

    public Component getConvertView(int layoutId) {
        return mConvertViews.get(layoutId).orElse(null);
    }

    public <V extends Component> V getView(int viewId) {
        Component view = mViews.get(viewId).orElse(null);
        if (view == null) {
            view = mConvertView.findComponentById(viewId);
            mViews.put(viewId, view);
        }
        return (V) view;
    }

    public void setPosition(int mPosition) {
        this.mPosition = mPosition;
    }

    public int getLayoutId() {
        return mLayoutId;
    }

    @Override
    public EasyLVHolder setText(int viewId, String value) {
        Text view = getView(viewId);
        view.setText(value);
        return this;
    }

    @Override
    public EasyLVHolder setTextColor(int viewId, int color) {
        Text view = getView(viewId);
        view.setTextColor(new Color(color));
        return this;
    }

    @Override
    public EasyLVHolder setTextColorRes(int viewId, int colorRes) {
        Text view = getView(viewId);
        view.setTextColor(new Color(mContext.getColor(colorRes)));
        return this;
    }

    @Override
    public EasyLVHolder setImageResource(int viewId, int imgResId) {
        Image view = getView(viewId);
        view.setPixelMap(imgResId);
        return this;
    }

    @Override
    public EasyLVHolder setBackgroundColor(int viewId, int color) {
        Component view = getView(viewId);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(color));
        view.setBackground(element);
        return this;
    }

    @Override
    public EasyLVHolder setBackgroundColorRes(int viewId, int colorRes) {
        Component view = getView(viewId);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(mContext.getColor(colorRes)));
        view.setBackground(element);
        return this;
    }

    @Override
    public EasyLVHolder setImageDrawable(int viewId, Element drawable) {
        Image view = getView(viewId);
        view.setImageElement(drawable);
        return this;
    }

    @Override
    public EasyLVHolder setImageDrawableRes(int viewId, int drawableRes) {
        Element drawable = null;
        try {
            mContext.getResourceManager().getElement(drawableRes);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return setImageDrawable(viewId, drawable);
    }

    @Override
    public EasyLVHolder setImageUrl(int viewId, String imgUrl) {
        // TODO: Use Glide/Picasso/ImageLoader/Fresco
        return this;
    }

    @Override
    public EasyLVHolder setImageBitmap(int viewId, PixelMap imgBitmap) {
        Image view = getView(viewId);
        view.setPixelMap(imgBitmap);
        return this;
    }

    @Override
    public EasyLVHolder setVisible(int viewId, boolean visible) {
        Component view = getView(viewId);
        view.setVisibility(visible ? Component.VISIBLE : Component.HIDE);
        return this;
    }

    @Override
    public EasyLVHolder setVisible(int viewId, int visible) {
        Component view = getView(viewId);
        view.setVisibility(visible);
        return this;
    }

    @Override
    public EasyLVHolder setTag(int viewId, Object tag) {
        Component view = getView(viewId);
        view.setTag(tag);
        return this;
    }

    @Override
    public EasyLVHolder setTag(int viewId, int key, Object tag) {
        Component view = getView(viewId);
        HashMap<Integer, Object> map = new HashMap<>();
        map.put(key, tag);
        view.setTag(map);
        return this;
    }

    @Override
    public EasyLVHolder setChecked(int viewId, boolean checked) {
        Checkbox view = getView(viewId);
        view.setChecked(checked);
        return this;
    }

    @Override
    public EasyLVHolder setAlpha(int viewId, float value) {
        getView(viewId).setAlpha(value);
        return this;
    }

    @Override
    public EasyLVHolder setTypeface(int viewId, Font typeface) {
        Text view = getView(viewId);
        view.setFont(typeface);
        return this;
    }

    @Override
    public EasyLVHolder setTypeface(Font typeface, int... viewIds) {
        for (int viewId : viewIds) {
            Text view = getView(viewId);
            view.setFont(typeface);
        }
        return this;
    }

    @Override
    public EasyLVHolder setClickedListener(int viewId, Component.ClickedListener listener) {
        Component view = getView(viewId);
        view.setClickedListener(listener);
        return this;
    }
}
