package com.yuyh.easyadapter.recyclerview;

import com.qy.reader.ResourceTable;
import com.qy.reader.widgets.RecyclerAdapter;
import com.yuyh.easyadapter.abslistview.EasyLVHolder;
import com.yuyh.easyadapter.helper.DataHelper;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;
import ohos.utils.PlainArray;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yuyh.
 * @date 2016/7/21.
 */
public abstract class EasyRVAdapter<T> extends RecyclerAdapter<EasyRVHolder> implements DataHelper<T> {

    /****
     * 头部相关
     */
    public static final int TYPE_HEADER = -1, TYPE_FOOTER = -2;
    private Component mHeaderView, mFooterView;
    private int headerViewId = -1, footerViewId = -2;

    protected Context mContext;
    protected List<T> mList;
    protected int[] layoutIds;
    protected LayoutScatter mLInflater;

    private PlainArray<Component> mConvertViews = new PlainArray<>();

    private OnItemClickListener<T> itemClickListener;
    private OnItemLongClickListener<T> itemLongClickListener;

    public EasyRVAdapter(Context context, List<T> list, int... layoutIds) {
        this.mContext = context;
        this.mList = list;
        this.layoutIds = layoutIds;
        this.mLInflater = LayoutScatter.getInstance(mContext);
    }

    @Override
    public EasyRVHolder onCreateViewHolder(ComponentContainer parent, int viewType) {
        if (mHeaderView != null && viewType == TYPE_HEADER) {
            return new EasyRVHolder(mContext, headerViewId, mHeaderView);
        }
        if (mFooterView != null && viewType == TYPE_FOOTER) {
            return new EasyRVHolder(mContext, footerViewId, mFooterView);
        }
        if (viewType < 0 || viewType > layoutIds.length) {
            throw new ArrayIndexOutOfBoundsException("layoutIndex");
        }
        if (layoutIds.length == 0) {
            throw new IllegalArgumentException("not layoutId");
        }
        int layoutId = layoutIds[viewType];
        Component view = mConvertViews.get(layoutId).orElse(null);
        if (view == null) {
            view = mLInflater.parse(layoutId, parent, false);
        }
        EasyRVHolder viewHolder = (EasyRVHolder) view.getTag();
        if (viewHolder == null || viewHolder.getLayoutId() != layoutId) {
            viewHolder = new EasyRVHolder(mContext, layoutId, view);
            return viewHolder;
        }
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(EasyRVHolder holder, int position) {
        if (getItemViewType(position) == TYPE_HEADER)
            return;
        if (getItemViewType(position) == TYPE_FOOTER)
            return;

        position = getPosition(position);
        final T item = mList.get(position);

        holder.itemViewProxy.setTag(ResourceTable.Id_tag_position, position);
        holder.itemViewProxy.setTag(ResourceTable.Id_tag_item, item);

        holder.getItemView().setClickedListener(clickListener);
        holder.getItemView().setLongClickedListener(longClickListener);

        onBindData(holder, position, item);
    }

    @Override
    public int getItemCount() {
        if (mHeaderView == null && mFooterView == null) {
            return mList == null ? 0 : mList.size();
        } else if (mHeaderView != null && mFooterView != null) {
            return mList == null ? 2 : mList.size() + 2;
        } else {
            return mList == null ? 1 : mList.size() + 1;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0 && mHeaderView != null) {
            return TYPE_HEADER;
        }
        if (position == getItemCount() - 1 && mFooterView != null) {
            return TYPE_FOOTER;
        }
        position = getPosition(position);
        return getLayoutIndex(position, mList.get(position));
    }

    private int getPosition(int position) {
        if (mHeaderView != null) {
            position = position - 1;
        }
        return position;
    }

    /**
     * 指定item布局样式在layoutIds的索引。默认为第一个
     *
     * @param position
     * @param item
     * @return
     */
    public int getLayoutIndex(int position, T item) {
        return 0;
    }

    /****
     * 设置头部
     * @param headerViewId
     */
    public Component setHeaderView(int headerViewId) {
        return setHeaderView(headerViewId, null);
    }

    public Component setHeaderView(int headerViewId, ComponentContainer parent) {
        mHeaderView = mLInflater.parse(headerViewId, parent, false);
        this.headerViewId = headerViewId;
        notifyDataSetItemInserted(0);
        return mHeaderView;
    }

    public void removeHeaderView() {
        if (mHeaderView != null) {
            mHeaderView = null;
            this.headerViewId = -1;
            notifyDataSetItemRemoved(0);
        }
    }

    public Component setFooterView(int footerViewId) {
        return setFooterView(footerViewId, null);
    }

    public Component setFooterView(int footerViewId, ComponentContainer parent) {
        mFooterView = mLInflater.parse(footerViewId, parent, false);
        this.footerViewId = footerViewId;
        notifyDataSetItemInserted(mList.size());
        return mFooterView;
    }

    public void removeFooterView() {
        if (mFooterView != null) {
            mFooterView = null;
            this.footerViewId = -2;
            notifyDataSetItemRemoved(mList.size() - 1);
        }
    }

    /****
     * 获取头部
     * @return
     */
    public Component getHeaderView() {
        return mHeaderView;
    }

    public Component getFooterView() {
        return mFooterView;
    }

    protected abstract void onBindData(EasyRVHolder viewHolder, int position, T item);

    @Override
    public boolean addAll(List<T> list) {
        boolean result = mList.addAll(list);
        notifyDataChanged();
        return result;
    }

    @Override
    public boolean addAll(int position, List list) {
        boolean result = mList.addAll(position, list);
        notifyDataChanged();
        return result;
    }

    @Override
    public void add(T data) {
        mList.add(data);
        notifyDataChanged();
    }

    @Override
    public void add(int position, T data) {
        mList.add(position, data);
        notifyDataChanged();
    }

    @Override
    public void clear() {
        mList.clear();
        notifyDataChanged();
    }

    @Override
    public boolean contains(T data) {
        return mList.contains(data);
    }

    @Override
    public T getData(int index) {
        return mList.get(index);
    }

    @Override
    public void modify(T oldData, T newData) {
        modify(mList.indexOf(oldData), newData);
    }

    @Override
    public void modify(int index, T newData) {
        mList.set(index, newData);
        notifyDataChanged();
    }

    @Override
    public boolean remove(T data) {
        boolean result = mList.remove(data);
        notifyDataChanged();
        return result;
    }

    @Override
    public void remove(int index) {
        mList.remove(index);
        notifyDataChanged();
    }

    private Component.ClickedListener clickListener = new Component.ClickedListener() {
        @Override
        public void onClick(Component v) {

            ViewHolder holder = (ViewHolder)v.getTag();

            int position = (Integer) holder.itemViewProxy.getTag(ResourceTable.Id_tag_position);
            T item = (T) holder.itemViewProxy.getTag(ResourceTable.Id_tag_item);

            if (itemClickListener != null) {
                itemClickListener.onItemClick(v, position, item);
            }
        }
    };

    private Component.LongClickedListener longClickListener = new Component.LongClickedListener() {
        @Override
        public void onLongClicked(Component v) {
            Map<Integer, Object> map = (Map<Integer, Object>) v.getTag();
            int position = (int) map.get(ResourceTable.Id_tag_position);
            T item = (T) map.get(ResourceTable.Id_tag_item);

            if (itemLongClickListener != null) {
                itemLongClickListener.onItemLongClick(v, position, item);
            }
        }
    };

    public void setOnItemClickListener(OnItemClickListener<T> itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void setOnItemLongClickListener(OnItemLongClickListener<T> itemLongClickListener) {
        this.itemLongClickListener = itemLongClickListener;
    }

    /****
     * RecyclerView Item 的点击事件
     * @param <T>
     */
    public interface OnItemClickListener<T> {
        void onItemClick(Component view, int position, T item);
    }

    /****
     * RecyclerView Item 的长按事件
     * @param <T>
     */
    public interface OnItemLongClickListener<T> {
        void onItemLongClick(Component view, int position, T item);
    }
}
