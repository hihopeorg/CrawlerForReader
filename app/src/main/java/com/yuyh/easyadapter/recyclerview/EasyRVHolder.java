package com.yuyh.easyadapter.recyclerview;

import com.qy.reader.widgets.RecyclerAdapter;
import com.yuyh.easyadapter.helper.ViewHelper;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Checkbox;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.utils.PlainArray;

import java.util.HashMap;

public class EasyRVHolder extends RecyclerAdapter.ViewHolder implements ViewHelper.RecyclerView<EasyRVHolder> {

    private PlainArray<Component> mViews = new PlainArray<>();

    private Component mConvertView;
    private int mLayoutId;
    protected Context mContext;

    public EasyRVHolder(Context context, int layoutId, Component itemView) {
        super(itemView);
        this.mContext = context;
        this.mLayoutId = layoutId;
        mConvertView = itemView;
        mConvertView.setTag(this);
    }

    public <V extends Component> V getView(int viewId) {
        Component view = mViews.get(viewId).orElse(null);
        if (view == null) {
            view = mConvertView.findComponentById(viewId);
            mViews.put(viewId, view);
        }
        return (V) view;
    }

    public int getLayoutId() {
        return mLayoutId;
    }

    /**
     * 获取item布局
     *
     * @return
     */
    public Component getItemView() {
        return mConvertView;
    }

    public EasyRVHolder setOnItemViewClickListener(Component.ClickedListener listener){
        mConvertView.setClickedListener(listener);
        return this;
    }

    public EasyRVHolder setOnItemViewLongClickListener(Component.LongClickedListener listener){
        mConvertView.setLongClickedListener(listener);
        return this;
    }

    @Override
    public EasyRVHolder setText(int viewId, String value) {
        Text view = getView(viewId);
        view.setText(value);
        return this;
    }

    @Override
    public EasyRVHolder setTextColor(int viewId, int color) {
        Text view = getView(viewId);
        view.setTextColor(new Color(color));
        return this;
    }

    @Override
    public EasyRVHolder setTextColorRes(int viewId, int colorRes) {
        Text view = getView(viewId);
        view.setTextColor(new Color(mContext.getColor(colorRes)));
        return this;
    }

    @Override
    public EasyRVHolder setImageResource(int viewId, int imgResId) {
        Image view = getView(viewId);
        view.setImageAndDecodeBounds(imgResId);
        return this;
    }

    @Override
    public EasyRVHolder setBackgroundColor(int viewId, int color) {
        Component view = getView(viewId);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(color));
        view.setBackground(element);
        return this;
    }

    @Override
    public EasyRVHolder setBackgroundColorRes(int viewId, int colorRes) {
        Component view = getView(viewId);
        ShapeElement element = new ShapeElement();
        element.setRgbColor(RgbColor.fromArgbInt(mContext.getColor(colorRes)));
        view.setBackground(element);
        return this;
    }

    @Override
    public EasyRVHolder setImageDrawable(int viewId, Element drawable) {
        Image view = getView(viewId);
        view.setImageElement(drawable);
        return this;
    }

    @Override
    public EasyRVHolder setImageDrawableRes(int viewId, int drawableRes) {
        Element drawable = null;
        try {
            mContext.getResourceManager().getElement(drawableRes);
        }catch (Exception e) {
            e.printStackTrace();
        }
        return setImageDrawable(viewId, drawable);
    }

    @Override
    public EasyRVHolder setImageUrl(int viewId, String imgUrl) {
        // TODO: Use Glide/Picasso/ImageLoader/Fresco
        return this;
    }

    @Override
    public EasyRVHolder setImageBitmap(int viewId, PixelMap imgBitmap) {
        Image view = getView(viewId);
        view.setPixelMap(imgBitmap);
        return this;
    }

    @Override
    public EasyRVHolder setVisible(int viewId, boolean visible) {
        Component view = getView(viewId);
        view.setVisibility(visible ? Component.VISIBLE : Component.HIDE);
        return this;
    }

    @Override
    public EasyRVHolder setVisible(int viewId, int visible) {
        Component view = getView(viewId);
        view.setVisibility(visible);
        return this;
    }

    @Override
    public EasyRVHolder setTag(int viewId, Object tag) {
        Component view = getView(viewId);
        view.setTag(tag);
        return this;
    }

    @Override
    public EasyRVHolder setTag(int viewId, int key, Object tag) {
        Component view = getView(viewId);
        HashMap<Integer, Object> map = new HashMap<>();
        map.put(key, tag);
        view.setTag(map);
        return this;
    }

    @Override
    public EasyRVHolder setChecked(int viewId, boolean checked) {
        Checkbox view = getView(viewId);
        view.setChecked(checked);
        return this;
    }

    @Override
    public EasyRVHolder setAlpha(int viewId, float value) {
        getView(viewId).setAlpha(value);
        return this;
    }

    @Override
    public EasyRVHolder setTypeface(int viewId, Font typeface) {
        Text view = getView(viewId);
        view.setFont(typeface);
        return this;
    }

    @Override
    public EasyRVHolder setTypeface(Font typeface, int... viewIds) {
        for (int viewId : viewIds) {
            Text view = getView(viewId);
            view.setFont(typeface);
        }
        return this;
    }

    @Override
    public EasyRVHolder setClickedListener(int viewId, Component.ClickedListener listener) {
        Component view = getView(viewId);
        view.setClickedListener(listener);
        return this;
    }
}
