package com.qy.reader.common.widgets;

import com.qy.reader.common.ResourceTable;
import com.qy.reader.common.base.BaseAbility;
import ohos.aafwk.ability.Ability;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.DirectionalLayout;
import ohos.agp.components.Image;
import ohos.agp.components.Text;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.components.element.VectorElement;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import java.lang.ref.WeakReference;

public class Sneaker implements Component.ClickedListener {

    private final int DEFAULT_VALUE = -100000;
    private int mIcon = DEFAULT_VALUE;
    private Element mIconDrawable = null;
    private int mBackgroundColor = DEFAULT_VALUE;
    private int mHeight = DEFAULT_VALUE;
    private int mIconColorFilterColor = DEFAULT_VALUE;
    private int mIconSize = 24;
    private String mTitle = "";
    private String mMessage = "";
    private int mTitleColor = DEFAULT_VALUE;
    private int mMessageColor = DEFAULT_VALUE;
    private boolean mAutoHide = true;
    private int mDuration = 3000;
    private static WeakReference<DirectionalLayout> layoutWeakReference;
    private static WeakReference<Ability> contextWeakReference;
    private boolean mIsCircular = false;
    private OnSneakerClickListener mListener = null;
    private Font mTypeFace = null;

    /**
     * Constructor
     *
     * @param ability
     */
    private Sneaker(Ability ability) {
        contextWeakReference = new WeakReference<>(ability);
    }

    /**
     * Create Sneaker with ability reference
     *
     * @param ability
     * @return
     */
    public static Sneaker with(Ability ability) {
        Sneaker sneaker = new Sneaker(ability);
        sneaker.setDefault();
        return sneaker;
    }

    /**
     * Hides the sneaker
     */
    public static void hide() {
        if (getLayout() != null) {
            AnimatorValue animatorValue = new AnimatorValue();
            animatorValue.setCurveType(Animator.CurveType.LINEAR);
            animatorValue.setDelay(200);
            animatorValue.setValueUpdateListener((animatorValue1, v) -> {
                int width = getLayout().getWidth();
                getLayout().setTranslationX(-width * v);
            });
            animatorValue.start();
            getAbilityDecorView().removeComponent(getLayout());
        }
    }

    /**
     * Return ability parent view
     *
     * @return
     */
    private static ComponentContainer getAbilityDecorView() {
        ComponentContainer decorView = null;

        decorView = (ComponentContainer) ((BaseAbility) getContext()).getRootComponent();

        return decorView;
    }

    /**
     * Sets the default values to the sneaker
     */
    private void setDefault() {
        mTitle = "";
        mIcon = DEFAULT_VALUE;
        mIconDrawable = null;
        mIconColorFilterColor = DEFAULT_VALUE;
        mIconSize = 24;
        mBackgroundColor = DEFAULT_VALUE;
        mAutoHide = true;
        mTitleColor = DEFAULT_VALUE;
        mMessageColor = DEFAULT_VALUE;
        mHeight = DEFAULT_VALUE;
        mIsCircular = false;
        mListener = null;
        mTypeFace = null;
    }

    /**
     * Return ability weak reference
     *
     * @return
     */
    private static Context getContext() {
        return contextWeakReference.get();
    }

    /**
     * Returns sneaker main layout weak reference
     *
     * @return
     */
    private static Component getLayout() {
        return layoutWeakReference.get();
    }

    /**
     * Sets the title of the sneaker
     *
     * @param title string value of title
     * @return
     */
    public Sneaker setTitle(String title) {
        mTitle = title;
        return this;
    }

    /**
     * Sets the title of the sneaker with color
     *
     * @param title string value of title
     * @param color Color resource for title text
     * @return
     */
    public Sneaker setTitle(String title, int color) {
        mTitle = title;
        if (getContext() != null) {
            try {
                mTitleColor = getContext().getColor(color);
            } catch (Exception e) {
                mTitleColor = color;
            }
        }
        return this;
    }

    /**
     * Sets the message to sneaker
     *
     * @param message String value of message
     * @return
     */
    public Sneaker setMessage(String message) {
        mMessage = message;
        return this;
    }

    /**
     * Sets the message to sneaker with color
     *
     * @param message String value of message
     * @param color   Color resource for message text
     * @return
     */
    public Sneaker setMessage(String message, int color) {
        mMessage = message;
        if (getContext() != null) {
            try {
                mMessageColor = getContext().getColor(color);
            } catch (Exception e) {
                mMessageColor = color;
            }
        }
        return this;
    }

    /**
     * Sets the icon to sneaker
     *
     * @param icon Icon resource for sneaker
     * @return
     */
    public Sneaker setIcon(int icon) {
        mIconDrawable = null;
        mIcon = icon;
        return this;
    }

    /**
     * Sets the icon to sneaker
     *
     * @param icon Icon drawable for sneaker
     * @return
     */
    public Sneaker setIcon(Element icon) {
        mIcon = DEFAULT_VALUE;
        mIconDrawable = icon;
        return this;
    }

    /**
     * Sets the icon to sneaker with circular option
     *
     * @param icon
     * @param isCircular If icon is round or not
     * @return
     */
    public Sneaker setIcon(int icon, boolean isCircular) {
        mIconDrawable = null;
        mIcon = icon;
        mIsCircular = isCircular;
        return this;
    }

    /**
     * Sets the icon to sneaker with circular option
     *
     * @param icon
     * @param isCircular If icon is round or not
     * @return
     */
    public Sneaker setIcon(Element icon, boolean isCircular) {
        mIcon = DEFAULT_VALUE;
        mIconDrawable = icon;
        mIsCircular = isCircular;
        return this;
    }

    public Sneaker setIcon(int icon, int tintColor) {
        mIconDrawable = null;
        mIcon = icon;
        if (getContext() != null) {
            try {
                mIconColorFilterColor = getContext().getColor(tintColor);
            } catch (Exception e) {
                mIconColorFilterColor = tintColor;
            }
        }
        return this;
    }

    public Sneaker setIcon(Element icon, int tintColor) {
        mIcon = DEFAULT_VALUE;
        mIconDrawable = icon;
        if (getContext() != null) {
            try {
                mIconColorFilterColor = getContext().getColor(tintColor);
            } catch (Exception e) {
                mIconColorFilterColor = tintColor;
            }
        }
        return this;
    }

    /**
     * Sets the icon to sneaker with circular option and icon tint
     *
     * @param icon
     * @param tintColor  Icon tint color
     * @param isCircular If icon is round or not
     * @return
     */
    public Sneaker setIcon(int icon, int tintColor, boolean isCircular) {
        mIconDrawable = null;
        mIcon = icon;
        mIsCircular = isCircular;
        if (getContext() != null) {
            try {
                mIconColorFilterColor = getContext().getColor(tintColor);
            } catch (Exception e) {
                mIconColorFilterColor = tintColor;
            }
        }
        return this;
    }

    /**
     * Sets the icon to sneaker with circular option and icon tint
     *
     * @param icon
     * @param tintColor  Icon tint color
     * @param isCircular If icon is round or not
     * @return
     */
    public Sneaker setIcon(Element icon, int tintColor, boolean isCircular) {
        mIcon = DEFAULT_VALUE;
        mIconDrawable = icon;
        mIsCircular = isCircular;
        if (getContext() != null) {
            try {
                mIconColorFilterColor = getContext().getColor(tintColor);
            } catch (Exception e) {
                mIconColorFilterColor = tintColor;
            }
        }
        return this;
    }

    /**
     * Sets the size of the icon.
     *
     * @param size New icon size.
     */
    public Sneaker setIconSize(int size) {
        mIconSize = size;
        return this;
    }

    /**
     * Disable/Enable auto hiding sneaker
     *
     * @param autoHide
     * @return
     */
    public Sneaker autoHide(boolean autoHide) {
        mAutoHide = autoHide;
        return this;
    }

    /**
     * Sets the height to sneaker
     *
     * @param height Height value for sneaker
     * @return
     */
    public Sneaker setHeight(int height) {
        mHeight = height;
        return this;
    }

    /**
     * Sets the duration for sneaker.
     * After this duration sneaker will disappear
     *
     * @param duration
     * @return
     */
    public Sneaker setDuration(int duration) {
        mDuration = duration;
        return this;
    }

    /**
     * Sets the click listener to sneaker
     *
     * @param listener
     * @return
     */
    public Sneaker setOnSneakerClickListener(OnSneakerClickListener listener) {
        mListener = listener;
        return this;
    }

    /**
     * Set font for title and message
     *
     * @param typeface
     * @return
     */
    public Sneaker setTypeface(Font typeface) {
        mTypeFace = typeface;
        return this;
    }

    /**
     * Shows sneaker with custom color
     *
     * @param backgroundColor Color resource for sneaker background color
     */
    public void sneak(int backgroundColor) {
        if (getContext() != null) {
            try {
                mBackgroundColor = getContext().getColor(backgroundColor);
            } catch (Exception e) {
                mBackgroundColor = backgroundColor;
            }
            sneakView();
        }
    }

    /**
     * Shows warning sneaker with fixed icon, background color and icon color.
     * Icons, background and text colors for this are not customizable
     */
    public void sneakWarning() {
        mBackgroundColor = Color.getIntColor("#ffc100");
        mTitleColor = Color.getIntColor("#000000");
        mMessageColor = Color.getIntColor("#000000");
        mIconColorFilterColor = Color.getIntColor("#000000");
        mIcon = ResourceTable.Graphic_ic_warning;

        if (getContext() != null)
            sneakView();
    }

    /**
     * Shows error sneaker with fixed icon, background color and icon color.
     * Icons, background and text colors for this are not customizable
     */
    public Sneaker sneakError() {
        mBackgroundColor = Color.getIntColor("#ff0000");
        mTitleColor = Color.getIntColor("#FFFFFF");
        mMessageColor = Color.getIntColor("#FFFFFF");
        mIconColorFilterColor = Color.getIntColor("#FFFFFF");
        mIcon = ResourceTable.Graphic_ic_error;

        if (getContext() != null)
            sneakView();
        return null;
    }

    /**
     * Shows success sneaker with fixed icon, background color and icon color.
     * Icons, background and text colors for this are not customizable
     */
    public Sneaker sneakSuccess() {
        mBackgroundColor = Color.getIntColor("#2bb600");
        mTitleColor = Color.getIntColor("#FFFFFF");
        mMessageColor = Color.getIntColor("#FFFFFF");
        mIconColorFilterColor = Color.getIntColor("#FFFFFF");
        mIcon = ResourceTable.Graphic_ic_success;

        if (getContext() != null)
            sneakView();
        return null;
    }

    /**
     * Creates the view and sneaks in
     */
    private void sneakView() {

        // Main layout
        DirectionalLayout layout = new DirectionalLayout(getContext());
        layoutWeakReference = new WeakReference<>(layout);

        DirectionalLayout.LayoutConfig layoutParams = new DirectionalLayout.LayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_PARENT,
                mHeight == DEFAULT_VALUE ? (convertToDp(56)) : convertToDp(mHeight));
        layout.setLayoutConfig(layoutParams);
        layout.setOrientation(DirectionalLayout.HORIZONTAL);
        layout.setAlignment(LayoutAlignment.VERTICAL_CENTER);
        layout.setPadding(46, 0, 46, 0);

        // Background color
        ShapeElement bg = new ShapeElement();
        bg.setRgbColor(RgbColor.fromArgbInt(mBackgroundColor));
        layout.setBackground(bg);

        // Icon
        // If icon is set
        if (mIcon != DEFAULT_VALUE || mIconDrawable != null) {
            if (!mIsCircular) {
                Image ivIcon = new Image(getContext());
                DirectionalLayout.LayoutConfig lp = new DirectionalLayout.LayoutConfig(convertToDp(mIconSize), convertToDp(mIconSize));
                ivIcon.setLayoutConfig(lp);

                if (mIcon == DEFAULT_VALUE) {
                    ivIcon.setImageElement(mIconDrawable);
                } else {
                    ivIcon.setImageElement(new VectorElement(getContext(), mIcon));
                }
                ivIcon.setClickable(false);
//                if (mIconColorFilterColor != DEFAULT_VALUE) {
//                    ivIcon.setColorFilter(mIconColorFilterColor);
//                }
                layout.addComponent(ivIcon);
            } else {
                CornerImageView ivIcon = new CornerImageView(getContext());
                ivIcon.setShapeMode(CornerImageView.SHAPE_MODE_CIRCLE);
                DirectionalLayout.LayoutConfig lp = new DirectionalLayout.LayoutConfig(convertToDp(mIconSize), convertToDp(mIconSize));
                ivIcon.setLayoutConfig(lp);

                if (mIcon == DEFAULT_VALUE) {
                    ivIcon.setImageElement(mIconDrawable);
                } else {
                    ivIcon.setImageElement(new VectorElement(getContext(), mIcon));
                }
                ivIcon.setClickable(false);
//                if (mIconColorFilterColor != DEFAULT_VALUE) {
//                    ivIcon.setColorFilter(mIconColorFilterColor);
//                }
                layout.addComponent(ivIcon);
            }
        }

        // Title and description
        DirectionalLayout textLayout = new DirectionalLayout(getContext());
        DirectionalLayout.LayoutConfig textLayoutParams = new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        textLayout.setLayoutConfig(textLayoutParams);
        textLayout.setOrientation(DirectionalLayout.VERTICAL);

        DirectionalLayout.LayoutConfig lpTv = new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        if (!mTitle.isEmpty()) {
            Text tvTitle = new Text(getContext());
            tvTitle.setLayoutConfig(lpTv);
            tvTitle.setTextAlignment(TextAlignment.VERTICAL_CENTER);
            if (!mMessage.isEmpty())
                tvTitle.setPadding(46, 26, 26, 0); // Top padding if there is message
            else
                tvTitle.setPadding(46, 0, 26, 0); // No top padding if there is no message
            if (mTitleColor != DEFAULT_VALUE)
                tvTitle.setTextColor(new Color(mTitleColor));

            // typeface
            if (mTypeFace != null)
                tvTitle.setFont(mTypeFace);

            tvTitle.setTextSize(14, Text.TextSizeType.FP);
            tvTitle.setText(mTitle);
            tvTitle.setClickable(false);
            textLayout.addComponent(tvTitle);
        }

        if (!mMessage.isEmpty()) {
            Text tvMessage = new Text(getContext());
            tvMessage.setLayoutConfig(lpTv);
            tvMessage.setTextAlignment(TextAlignment.VERTICAL_CENTER);
            if (!mTitle.isEmpty())
                tvMessage.setPadding(46, 0, 26, 26); // Bottom padding if there is title
            else
                tvMessage.setPadding(46, 0, 26, 0); // No bottom padding if there is no title
            if (mMessageColor != DEFAULT_VALUE)
                tvMessage.setTextColor(new Color(mMessageColor));

            // typeface
            if (mTypeFace != null)
                tvMessage.setFont(mTypeFace);

            tvMessage.setTextSize(12, Text.TextSizeType.FP);
            tvMessage.setText(mMessage);
            tvMessage.setClickable(false);
            textLayout.addComponent(tvMessage);
        }
        layout.addComponent(textLayout);
        layout.setId(ResourceTable.Id_sneaker_main_layout);


        final ComponentContainer viewGroup = getAbilityDecorView();
        getExistingOverlayInViewAndRemove(viewGroup);

        layout.setClickedListener(this);
        viewGroup.addComponent(layout);

        AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setCurveType(Animator.CurveType.LINEAR);
        animatorValue.setDelay(200);
        animatorValue.setValueUpdateListener((animatorValue1, v) -> {
            int height = layout.getHeight();
            layout.setTranslationY(-height * (1 - v));
        });
        layout.setTranslationY(-layout.getHeight());
        animatorValue.start();
        if (mAutoHide) {
            EventHandler handler = new EventHandler(EventRunner.getMainEventRunner());
            handler.removeAllEvent();
            handler.postTask(new Runnable() {
                @Override
                public void run() {
                    AnimatorValue animatorValue = new AnimatorValue();
                    animatorValue.setCurveType(Animator.CurveType.LINEAR);
                    animatorValue.setDelay(200);
                    animatorValue.setValueUpdateListener((animatorValue1, v) -> {
                        int height = getLayout().getHeight();
                        getLayout().setTranslationY(-height * v);
                        if (Math.abs(v - 1f) < 0.000001f) {
                            viewGroup.removeComponent(getLayout());
                        }
                    });
                    animatorValue.start();
                }
            }, mDuration);
        }
    }

    /**
     * Gets the existing sneaker and removes before adding new one
     *
     * @param parent
     */
    public void getExistingOverlayInViewAndRemove(ComponentContainer parent) {

        for (int i = parent.getChildCount()-1; i >= 0; i--) {

            Component child = parent.getComponentAt(i);
            if(child==null) {
                continue;
            }
            if (child.getId() == ResourceTable.Id_sneaker_main_layout) {
                parent.removeComponent(child);
                continue;
            }
            if (child instanceof ComponentContainer) {
                getExistingOverlayInViewAndRemove((ComponentContainer) child);
            }
        }
    }

    private int convertToDp(float sizeInDp) {
        float scale = DisplayManager.getInstance().getDefaultDisplay(getContext()).get().getAttributes().scalDensity;
        return (int) (sizeInDp * scale + 0.5f);
    }

    /**
     * Sneaker on click
     *
     * @param view
     */
    @Override
    public void onClick(Component view) {
        if (mListener != null) {
            mListener.onSneakerClick(view);
        }
        AnimatorValue animatorValue = new AnimatorValue();
        animatorValue.setCurveType(Animator.CurveType.LINEAR);
        animatorValue.setDelay(200);
        animatorValue.setValueUpdateListener((animatorValue1, v) -> {
            int width = getLayout().getWidth();
            getLayout().setTranslationX(-width * v);
        });
        animatorValue.start();
        getAbilityDecorView().removeComponent(getLayout());
    }

    public interface OnSneakerClickListener {
        void onSneakerClick(Component view);
    }
}