package com.qy.reader.common.utils;

import ohos.app.Context;
import ohos.media.image.ImagePacker;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Rect;
import ohos.media.image.common.ScaleMode;
import ohos.media.image.common.Size;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author yuyh.
 * @date 17/3/27.
 */
public class BitmapUtils {

    /**
     * 根据给定的宽和高进行拉伸
     *
     * @param origin    原图
     * @param newWidth  新图的宽
     * @param newHeight 新图的高
     * @return new PixelMap
     */
    public static PixelMap scaleBitmap(PixelMap origin, int newWidth, int newHeight) {
        if (origin == null) {
            return null;
        }
        int height = origin.getImageInfo().size.height;
        int width = origin.getImageInfo().size.width;
        PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
        options.size = new Size(newWidth, newHeight);
        options.scaleMode = ScaleMode.FIT_TARGET_SIZE;
        options.useSourceIfMatch = true;
        options.pixelFormat = origin.getImageInfo().pixelFormat;
        options.alphaType = origin.getImageInfo().alphaType;
        PixelMap newBM = PixelMap.create(origin, new Rect(0, 0, width, height), options);
        if (!origin.isReleased()) {
            origin.release();
        }
        return newBM;
    }

    public static PixelMap readBitmap(Context context, int resId) {
        //获取资源图片
        try {
            InputStream is = context.getResourceManager().getResource(resId);
            return ImageSource.create(is, null).createPixelmap(null);
        }catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void recycler(PixelMap pixelMap) {
        if (pixelMap != null && !pixelMap.isReleased()) {
            pixelMap.release();
        }
        pixelMap = null;
    }

    public static void saveBitmap(PixelMap pixelMap, File saveFile) {
        if (saveFile.exists()) {
            saveFile.delete();
        }
        ImagePacker.PackingOptions options = new ImagePacker.PackingOptions();
        options.format = "image/png";
        options.quality = 90;
        ImagePacker imagePacker = ImagePacker.create();
        try {
            FileOutputStream out = new FileOutputStream(saveFile);
            imagePacker.initializePacking(out, options);
            imagePacker.addImage(pixelMap);
            imagePacker.finalizePacking();
            out.flush();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static byte[] bitmap2Bytes(PixelMap pixelMap) {
        if (pixelMap == null)
            return null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImagePacker.PackingOptions options = new ImagePacker.PackingOptions();
        options.format = "image/jpeg";
        options.quality = 100;
        ImagePacker imagePacker = ImagePacker.create();
        imagePacker.initializePacking(baos, options);
        imagePacker.addImage(pixelMap);
        imagePacker.finalizePacking();
        return baos.toByteArray();
    }

    public static byte[] bitmap2Bytes(PixelMap pixelMap, int maxkb) {
        if (pixelMap == null)
            return null;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImagePacker.PackingOptions options = new ImagePacker.PackingOptions();
        options.format = "image/jpeg";
        options.quality = 100;
        ImagePacker imagePacker = ImagePacker.create();
        imagePacker.initializePacking(baos, options);
        imagePacker.addImage(pixelMap);
        imagePacker.finalizePacking();

        while (baos.toByteArray().length / 1024 > maxkb) {
            if (options.quality <= 10) {
                break;
            }
            options.quality -= 10;
            baos.reset();
            imagePacker.initializePacking(baos, options);
            imagePacker.addImage(pixelMap);
            imagePacker.finalizePacking();
        }
        return baos.toByteArray();
    }

    /**
     * 根据指定的图像路径和大小来获取缩略图
     * 此方法有两点好处：
     * 1. 使用较小的内存空间，第一次获取的bitmap实际上为null，只是为了读取宽度和高度，
     * 第二次读取的bitmap是根据比例压缩过的图像，第三次读取的bitmap是所要的缩略图。
     * 2. 缩略图对于原图像来讲没有拉伸，这里使用了2.2版本的新工具ThumbnailUtils，使
     * 用这个工具生成的图像不会被拉伸。
     *
     * @param imagePath 图像的路径
     * @param width     指定输出图像的宽度
     * @param height    指定输出图像的高度
     * @return 生成的缩略图
     */
    public static PixelMap getImageThumbnail(String imagePath, int width, int height) {
        ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
        decodingOptions.desiredPixelFormat = PixelFormat.ARGB_8888;
        decodingOptions.desiredSize = new Size(width, height);
        PixelMap thumb = ImageSource.create(imagePath, null).createPixelmap(decodingOptions);
        return thumb;
    }

    public static PixelMap rotateBitmap(String src, float rotateDegrees) {
        ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
        decodingOptions.desiredPixelFormat = PixelFormat.ARGB_8888;
        decodingOptions.rotateDegrees = rotateDegrees;
        PixelMap pixelMap = ImageSource.create(src, null).createPixelmap(decodingOptions);
        return pixelMap;
    }

    public static PixelMap compressBitmap(PixelMap image, int maxkb) {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImagePacker.PackingOptions options = new ImagePacker.PackingOptions();
        options.format = "image/jpeg";
        options.quality = 90;
        ImagePacker imagePacker = ImagePacker.create();
        imagePacker.initializePacking(baos, options);
        imagePacker.addImage(image);
        imagePacker.finalizePacking();

        while (baos.toByteArray().length / 1024 > maxkb) {
            if (options.quality > 10) {
                baos.reset();// 重置baos即清空baos
                options.quality -= 10;// 每次都减少10
                imagePacker.initializePacking(baos, options);
                imagePacker.addImage(image);
                imagePacker.finalizePacking();
            } else {
                break;
            }
        }

        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());// 把压缩后的数据baos存放到ByteArrayInputStream中

        recycler(image);

        return ImageSource.create(isBm, null).createPixelmap(null);
    }
}
