package com.qy.reader.common.utils;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

/**
 * Created by yuyuhang on 2017/12/4.
 */
public class HandlerFactory {

    private static EventHandler sUIHandler;

    static {
        sUIHandler = new EventHandler(EventRunner.getMainEventRunner());
    }

    public static EventHandler getUIHandler() {
        return sUIHandler;
    }
}
