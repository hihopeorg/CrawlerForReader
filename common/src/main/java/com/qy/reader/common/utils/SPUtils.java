package com.qy.reader.common.utils;

import com.qy.reader.common.Global;
import ohos.app.Context;
import ohos.data.DatabaseHelper;
import ohos.data.preferences.Preferences;
import ohos.utils.LightweightMap;

import java.util.Collections;
import java.util.Map;
import java.util.Set;

public final class SPUtils {

    private static LightweightMap<String, SPUtils> SP_UTILS_MAP = new LightweightMap<>();
    private Preferences sp;

    /**
     * 获取 SP 实例
     *
     * @return {@link SPUtils}
     */
    public static SPUtils getInstance() {
        return getInstance("qy-ohos");
    }

    /**
     * 获取 SP 实例
     *
     * @param spName sp 名
     * @return {@link SPUtils}
     */
    public static SPUtils getInstance(String spName) {
        if (isSpace(spName)) spName = "spUtils";
        SPUtils spUtils = SP_UTILS_MAP.get(spName);
        if (spUtils == null) {
            spUtils = new SPUtils(spName);
            SP_UTILS_MAP.put(spName, spUtils);
        }
        return spUtils;
    }

    private SPUtils(final String spName) {
        sp = new DatabaseHelper(Global.getApplication().getApplicationContext()).getPreferences(spName);
    }

    /**
     * SP 中写入 String
     *
     * @param key   键
     * @param value 值
     */
    public void put(final String key, final String value) {
        put(key, value, false);
    }

    /**
     * SP 中写入 String
     *
     * @param key      键
     * @param value    值
     * @param isCommit {@code true}: {@link Preferences#flush()}<br>
     *                 {@code false}: nothing
     */
    public void put(final String key,
                    final String value,
                    final boolean isCommit) {
        if (isCommit) {
            sp.putString(key, value).flush();
        } else {
            sp.putString(key, value);
        }
    }

    /**
     * SP 中读取 String
     *
     * @param key 键
     * @return 存在返回对应值，不存在返回默认值{@code ""}
     */
    public String getString(final String key) {
        return getString(key, "");
    }

    /**
     * SP 中读取 String
     *
     * @param key          键
     * @param defaultValue 默认值
     * @return 存在返回对应值，不存在返回默认值{@code defaultValue}
     */
    public String getString(final String key, final String defaultValue) {
        return sp.getString(key, defaultValue);
    }

    /**
     * SP 中写入 int
     *
     * @param key   键
     * @param value 值
     */
    public void put(final String key, final int value) {
        put(key, value, false);
    }

    /**
     * SP 中写入 int
     *
     * @param key      键
     * @param value    值
     * @param isCommit {@code true}: {@link Preferences#flush()}<br>
     *                 {@code false}: nothing
     */
    public void put(final String key, final int value, final boolean isCommit) {
        if (isCommit) {
            sp.putInt(key, value).flush();
        } else {
            sp.putInt(key, value);
        }
    }

    /**
     * SP 中读取 int
     *
     * @param key 键
     * @return 存在返回对应值，不存在返回默认值-1
     */
    public int getInt(final String key) {
        return getInt(key, -1);
    }

    /**
     * SP 中读取 int
     *
     * @param key          键
     * @param defaultValue 默认值
     * @return 存在返回对应值，不存在返回默认值{@code defaultValue}
     */
    public int getInt(final String key, final int defaultValue) {
        return sp.getInt(key, defaultValue);
    }

    /**
     * SP 中写入 long
     *
     * @param key   键
     * @param value 值
     */
    public void put(final String key, final long value) {
        put(key, value, false);
    }

    /**
     * SP 中写入 long
     *
     * @param key      键
     * @param value    值
     * @param isCommit {@code true}: {@link Preferences#flush()}<br>
     *                 {@code false}: nothing
     */
    public void put(final String key, final long value, final boolean isCommit) {
        if (isCommit) {
            sp.putLong(key, value).flush();
        } else {
            sp.putLong(key, value);
        }
    }

    /**
     * SP 中读取 long
     *
     * @param key 键
     * @return 存在返回对应值，不存在返回默认值-1
     */
    public long getLong(final String key) {
        return getLong(key, -1L);
    }

    /**
     * SP 中读取 long
     *
     * @param key          键
     * @param defaultValue 默认值
     * @return 存在返回对应值，不存在返回默认值{@code defaultValue}
     */
    public long getLong(final String key, final long defaultValue) {
        return sp.getLong(key, defaultValue);
    }

    /**
     * SP 中写入 float
     *
     * @param key   键
     * @param value 值
     */
    public void put(final String key, final float value) {
        put(key, value, false);
    }

    /**
     * SP 中写入 float
     *
     * @param key      键
     * @param value    值
     * @param isCommit {@code true}: {@link Preferences#flush()}<br>
     *                 {@code false}: nothing
     */
    public void put(final String key, final float value, final boolean isCommit) {
        if (isCommit) {
            sp.putFloat(key, value).flush();
        } else {
            sp.putFloat(key, value);
        }
    }

    /**
     * SP 中读取 float
     *
     * @param key 键
     * @return 存在返回对应值，不存在返回默认值-1
     */
    public float getFloat(final String key) {
        return getFloat(key, -1f);
    }

    /**
     * SP 中读取 float
     *
     * @param key          键
     * @param defaultValue 默认值
     * @return 存在返回对应值，不存在返回默认值{@code defaultValue}
     */
    public float getFloat(final String key, final float defaultValue) {
        return sp.getFloat(key, defaultValue);
    }

    /**
     * SP 中写入 boolean
     *
     * @param key   键
     * @param value 值
     */
    public void put(final String key, final boolean value) {
        put(key, value, false);
    }

    /**
     * SP 中写入 boolean
     *
     * @param key      键
     * @param value    值
     * @param isCommit {@code true}: {@link Preferences#flush()}<br>
     *                 {@code false}: nothing
     */
    public void put(final String key, final boolean value, final boolean isCommit) {
        if (isCommit) {
            sp.putBoolean(key, value).flush();
        } else {
            sp.putBoolean(key, value);
        }
    }

    /**
     * SP 中读取 boolean
     *
     * @param key 键
     * @return 存在返回对应值，不存在返回默认值{@code false}
     */
    public boolean getBoolean(final String key) {
        return getBoolean(key, false);
    }

    /**
     * SP 中读取 boolean
     *
     * @param key          键
     * @param defaultValue 默认值
     * @return 存在返回对应值，不存在返回默认值{@code defaultValue}
     */
    public boolean getBoolean(final String key, final boolean defaultValue) {
        return sp.getBoolean(key, defaultValue);
    }

    /**
     * SP 中写入 String 集合
     *
     * @param key    键
     * @param values 值
     */
    public void put(final String key, final Set<String> values) {
        put(key, values, false);
    }

    /**
     * SP 中写入 String 集合
     *
     * @param key      键
     * @param values   值
     * @param isCommit {@code true}: {@link Preferences#flush()}<br>
     *                 {@code false}: nothing
     */
    public void put(final String key,
                    final Set<String> values,
                    final boolean isCommit) {
        if (isCommit) {
            sp.putStringSet(key, values).flush();
        } else {
            sp.putStringSet(key, values);
        }
    }

    /**
     * SP 中读取 StringSet
     *
     * @param key 键
     * @return 存在返回对应值，不存在返回默认值{@code Collections.<String>emptySet()}
     */
    public Set<String> getStringSet(final String key) {
        return getStringSet(key, Collections.<String>emptySet());
    }

    /**
     * SP 中读取 StringSet
     *
     * @param key          键
     * @param defaultValue 默认值
     * @return 存在返回对应值，不存在返回默认值{@code defaultValue}
     */
    public Set<String> getStringSet(final String key,
                                    final Set<String> defaultValue) {
        return sp.getStringSet(key, defaultValue);
    }

    /**
     * SP 中获取所有键值对
     *
     * @return Map 对象
     */
    public Map<String, ?> getAll() {
        return sp.getAll();
    }

    /**
     * SP 中是否存在该 key
     *
     * @param key 键
     * @return {@code true}: 存在<br>{@code false}: 不存在
     */
    public boolean contains(final String key) {
        return sp.hasKey(key);
    }

    /**
     * SP 中移除该 key
     *
     * @param key 键
     */
    public void remove(final String key) {
        remove(key, false);
    }

    /**
     * SP 中移除该 key
     *
     * @param key      键
     * @param isCommit {@code true}: {@link Preferences#flush()}<br>
     *                 {@code false}: nothing
     */
    public void remove(final String key, final boolean isCommit) {
        if (isCommit) {
            sp.delete(key).flush();
        } else {
            sp.delete(key);
        }
    }

    /**
     * SP 中清除所有数据
     */
    public void clear() {
        clear(false);
    }

    /**
     * SP 中清除所有数据
     *
     * @param isCommit {@code true}: {@link Preferences#flush()}<br>
     *                 {@code false}: nothing
     */
    public void clear(final boolean isCommit) {
        if (isCommit) {
            sp.clear().flush();
        } else {
            sp.clear();
        }
    }

    private static boolean isSpace(final String s) {
        if (s == null) return true;
        for (int i = 0, len = s.length(); i < len; ++i) {
            if (!Character.isWhitespace(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }
}