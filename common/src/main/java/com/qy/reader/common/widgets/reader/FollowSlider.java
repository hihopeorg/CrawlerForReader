package com.qy.reader.common.widgets.reader;

import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.PixelMapHolder;

/**
 * 滑动翻页
 *
 * @author yuyh.
 * @date 17/3/25.
 */
public class FollowSlider extends BaseSlider {

    private Path mPath;
    private Paint paint = new Paint();

    public FollowSlider() {
        mPath = new Path();
    }

    @Override
    void drawCurrentPageArea(Canvas canvas) {
        mPath.reset();

        canvas.save();

        PixelMapHolder holder = new PixelMapHolder(mReadView.getCurPageBitmap());

        // 手指滑动状态
        if (mMode == MODE_MOVE && mDirection != MOVE_NO_RESULT) { // 有效滑动（手指触摸滑动过程）
            mVelocityTracker.calculateCurrentVelocity(1000);
            if (mDirection == MOVE_TO_LEFT) {
                mPath.addRect(0, 0, mScreenWidth - distance, mScreenHeight, Path.Direction.CLOCK_WISE);
                canvas.clipPath(mPath, Canvas.ClipOp.INTERSECT);
                canvas.drawPixelMapHolder(holder, -distance, 0, paint);
            } else if (mDirection == MOVE_TO_RIGHT) {
                mPath.addRect(-distance, 0, mScreenWidth, mScreenHeight, Path.Direction.CLOCK_WISE);
                canvas.clipPath(mPath, Canvas.ClipOp.INTERSECT);
                canvas.drawPixelMapHolder(holder, -distance, 0, paint);
            }
        } else if (autoScrollLeft != SCROLL_NONE) { // 自动左滑
            mPath.addRect(0, 0, -scrollDistance, mScreenHeight, Path.Direction.CLOCK_WISE);
            canvas.clipPath(mPath, Canvas.ClipOp.INTERSECT);
            canvas.drawPixelMapHolder(holder, -mScreenWidth - scrollDistance, 0, paint);
        } else if (autoScrollRight != SCROLL_NONE) { // 自动右滑
            mPath.addRect(-scrollDistance, 0, mScreenWidth, mScreenHeight, Path.Direction.CLOCK_WISE);
            canvas.clipPath(mPath, Canvas.ClipOp.INTERSECT);
            canvas.drawPixelMapHolder(holder, -scrollDistance, 0, paint);
        } else { // 初始状态或无效滑动（eg: 先往左滑，又往右滑到初始状态，继续往右就不动）
            mPath.addRect(0, 0, mScreenWidth, mScreenHeight, Path.Direction.CLOCK_WISE);
            canvas.clipPath(mPath, Canvas.ClipOp.INTERSECT);
            canvas.drawPixelMapHolder(holder, 0, 0, paint);
        }
        canvas.restore();
    }

    @Override
    void drawTempPageArea(Canvas canvas) {
        mPath.reset();

        canvas.save();

        if (mMode == MODE_MOVE && mDirection != MOVE_NO_RESULT) {
            mVelocityTracker.calculateCurrentVelocity(1000);
            if (mDirection == MOVE_TO_LEFT) {
                mPath.addRect(mScreenWidth - distance, 0, mScreenWidth, mScreenHeight, Path.Direction.CLOCK_WISE);
                canvas.clipPath(mPath, Canvas.ClipOp.INTERSECT);
                PixelMapHolder holder = new PixelMapHolder(mReadView.getNextPageBitmap());
                canvas.drawPixelMapHolder(holder, mScreenWidth - distance, 0, paint);
            } else if (mDirection == MOVE_TO_RIGHT) {
                mPath.addRect(-mScreenWidth - distance, 0, -distance, mScreenHeight, Path.Direction.CLOCK_WISE);
                canvas.clipPath(mPath, Canvas.ClipOp.INTERSECT);
                PixelMapHolder holder = new PixelMapHolder(mReadView.getPrePageBitmap());
                canvas.drawPixelMapHolder(holder, -mScreenWidth - distance, 0, paint);
            }
        } else {
            if (autoScrollLeft != SCROLL_NONE) {
                mPath.addRect(-scrollDistance, 0, mScreenWidth, mScreenHeight, Path.Direction.CLOCK_WISE);
                canvas.clipPath(mPath, Canvas.ClipOp.INTERSECT);
                PixelMapHolder holder = new PixelMapHolder(mReadView.getNextPageBitmap());
                canvas.drawPixelMapHolder(holder, -scrollDistance, 0, paint);
            } else if (autoScrollRight != SCROLL_NONE) {
                mPath.addRect(-mScreenWidth - scrollDistance, 0, -scrollDistance, mScreenHeight, Path.Direction.CLOCK_WISE);
                canvas.clipPath(mPath, Canvas.ClipOp.INTERSECT);
                PixelMapHolder holder = new PixelMapHolder(mReadView.getPrePageBitmap());
                canvas.drawPixelMapHolder(holder, -mScreenWidth - scrollDistance, 0, paint);
            }
        }
        canvas.restore();
    }

    @Override
    void drawShadow(Canvas canvas) {

    }
}
