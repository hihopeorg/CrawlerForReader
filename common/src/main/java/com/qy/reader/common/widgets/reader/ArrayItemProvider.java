/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qy.reader.common.widgets.reader;

import ohos.agp.components.BaseItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

import java.util.ArrayList;

public class ArrayItemProvider<T> extends BaseItemProvider {

    private final Context context;
    private final int layoutResId;
    private final ArrayList<T> items = new ArrayList<>();

    public ArrayItemProvider(Context context, int layoutResId) {
        this.context = context;
        this.layoutResId = layoutResId;
    }

    public void add(T item) {
        items.add(item);
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int i) {
        return items.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public Component getComponent(int position, Component convertComponent, ComponentContainer parent) {
        if (convertComponent == null) {
            convertComponent = LayoutScatter.getInstance(context).parse(layoutResId, parent, false);
        }
        return convertComponent;
    }
}