package com.qy.reader.common.widgets;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.PixelMapHolder;
import ohos.agp.render.PixelMapShader;
import ohos.agp.render.Shader;
import ohos.agp.utils.Color;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

/**
 * 圆角ImageView
 * <p>
 * Created by yuyuhang on 2018/1/10.
 */
public class CornerImageView extends Image implements Component.DrawTask {

    public static final int SHAPE_MODE_ROUND_RECT = 1;
    public static final int SHAPE_MODE_CIRCLE = 2;

    private int mShapeMode = 0;
    private float mRadius = 0;
    private int mStrokeColor = 0x26000000;
    private float mStrokeWidth = 0;
    private boolean mShapeChanged;

    private Path mPath;
    private Paint mPaint, mStrokePaint, mPathPaint;

    private RectFloat mImageBounds = new RectFloat();

    private PathExtension mExtension;

    public CornerImageView(Context context) {
        super(context);
        init(null);
    }

    public CornerImageView(Context context, AttrSet attrSet) {
        super(context, attrSet);
        init(attrSet);
    }

    public CornerImageView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }

    private void init(AttrSet attrs) {
        if (attrs != null) {
            attrs.getAttr("shape").ifPresent(value-> mShapeMode = value.getIntegerValue());
            attrs.getAttr("radius").ifPresent(value-> mRadius = value.getIntegerValue());
            attrs.getAttr("stroke_width").ifPresent(value-> mStrokeWidth = value.getIntegerValue());
            attrs.getAttr("stroke_color").ifPresent(value-> mStrokeColor = value.getIntegerValue());
        }
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setFilterBitmap(true);
        mPaint.setStyle(Paint.Style.FILL_STYLE);
        mPaint.setColor(Color.BLACK);

        mStrokePaint = new Paint();
        mStrokePaint.setAntiAlias(true);
        mStrokePaint.setFilterBitmap(true);
        mStrokePaint.setStyle(Paint.Style.STROKE_STYLE);
        mStrokePaint.setStrokeWidth(mStrokeWidth);
        mStrokePaint.setColor(new Color(mStrokeColor));

        mPathPaint = new Paint();
        mPathPaint.setAntiAlias(true);
        mPathPaint.setFilterBitmap(true);
        mPathPaint.setColor(Color.BLACK);

        mPath = new Path();

        addDrawTask(this);
    }

    @Override
    public void postLayout() {
        super.postLayout();

        int width = getEstimatedWidth();
        int height = getEstimatedHeight();

        switch (mShapeMode) {
            case SHAPE_MODE_ROUND_RECT:
                break;
            case SHAPE_MODE_CIRCLE:
                int min = Math.min(width, height);
                mRadius = (float) min / 2;
                break;
        }

        mImageBounds.modify(mStrokeWidth, mStrokeWidth, width-mStrokeWidth, height-mStrokeWidth);

        PixelMapHolder holder = new PixelMapHolder(getPixelMap());
        PixelMapShader shader = new PixelMapShader(holder, Shader.TileMode.REPEAT_TILEMODE, Shader.TileMode.REPEAT_TILEMODE);
        mPaint.setShader(shader, Paint.ShaderType.PIXELMAP_SHADER);

        if (mExtension != null) {
            mExtension.onLayout(mPath, width, height);
        }
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        RectFloat rect = new RectFloat(0, 0, getEstimatedWidth(), getEstimatedHeight());
        canvas.drawRoundRect(rect, mRadius, mRadius, mStrokePaint);

        if (mExtension != null) {
            canvas.drawPath(mPath, mPathPaint);
        }

        switch (mShapeMode) {
            case SHAPE_MODE_ROUND_RECT:
            case SHAPE_MODE_CIRCLE:
                canvas.translate(mStrokeWidth, mStrokeWidth);
                canvas.drawRoundRect(mImageBounds, mRadius, mRadius, mPaint);
                break;
        }
    }

    public void setExtension(PathExtension extension) {
        mExtension = extension;
        postLayout();
    }

    public void setStroke(int strokeColor, float strokeWidth) {
        if (mStrokeWidth <= 0) return;

        if (mStrokeWidth != strokeWidth) {
            mStrokeWidth = strokeWidth;
            mStrokePaint.setStrokeWidth(mStrokeWidth);
            invalidate();
        }

        if (mStrokeColor != strokeColor) {
            mStrokeColor = strokeColor;
            mStrokePaint.setColor(new Color(mStrokeColor));
            invalidate();
        }
    }

    public void setStrokeColor(int strokeColor) {
        setStroke(strokeColor, mStrokeWidth);
    }

    public void setStrokeWidth(float strokeWidth) {
        setStroke(mStrokeColor, strokeWidth);
    }

    public void setShape(int shapeMode, float radius) {
        mShapeChanged = mShapeMode != shapeMode || (Math.abs(mRadius - radius) > 0.000001f);

        if (mShapeChanged) {
            mShapeMode = shapeMode;
            mRadius = radius;
            postLayout();
        }
    }

    public void setShapeMode(int shapeMode) {
        setShape(shapeMode, mRadius);
    }

    public void setShapeRadius(float radius) {
        setShape(mShapeMode, radius);
    }

    public interface PathExtension {
        void onLayout(Path path, int width, int height);
    }

}