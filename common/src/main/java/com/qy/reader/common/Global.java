package com.qy.reader.common;

import ohos.aafwk.ability.AbilityPackage;

/**
 * Created by yuyuhang on 2018/1/8.
 */
public class Global {

    private static AbilityPackage application;

    public static void init(AbilityPackage application) {
        Global.application = application;
    }

    public static AbilityPackage getApplication() {
        return application;
    }
}
