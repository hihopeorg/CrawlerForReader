package com.qy.reader.common.utils;

import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.IntentParams;
import ohos.agp.utils.TextTool;
import ohos.app.Context;
import ohos.utils.PacMap;
import ohos.utils.net.Uri;

import java.net.URI;
import java.net.URLDecoder;

/**
 * 页面路由跳转
 * <p>
 * Created by yuyuhang on 2018/1/13.
 */
public class Nav {

    private static final int INVALID = -1;

    private Context mContext;

    private PacMap mBundle;
    private int flags = INVALID;
    private int inAnim = INVALID, outAnim = INVALID;

    public Nav(Context context) {
        this.mContext = context;
        mBundle = new PacMap();
    }

    public static Nav from(Context context) {
        return new Nav(context);
    }

    public Nav setExtras(PacMap bundle) {
        if (bundle != null) {
            mBundle.putAll(bundle);
        }
        return this;
    }

    public Nav setFlags(int flags) {
        this.flags = flags;
        return this;
    }

    public Nav overridePendingTransition(int enterAnim, int exitAnim) {
        this.inAnim = enterAnim;
        this.outAnim = exitAnim;
        return this;
    }

    public void start(String url) {
        start(url, -1);
    }

    public void start(String url, int reqCode) {
        if (TextTool.isNullOrEmpty(url)) {
            recycle();
            return;
        }

        try {
            URI uri = new URI(url);

            String schema = uri.getScheme();
            String query = uri.getQuery();
            if (!TextTool.isNullOrEmpty(query)) {
                String[] params = query.split("&");
                for (String param : params) {
                    String[] kv = param.split("=");
                    if (kv.length == 2) {
                        String key = kv[0];
                        String value = kv[1];
                        mBundle.putString(key, URLDecoder.decode(value, "UTF-8"));
                    }
                }
            }

            if (schema.equals("http") || schema.equals("https")) {

            } else {
                Intent intent = new Intent();
                IntentParams intentParams = new IntentParams();
                mBundle.getAll().forEach(intentParams::setParam);
                intent.setParams(intentParams);
                Intent.OperationBuilder builder = new Intent.OperationBuilder()
                        .withUri(Uri.parse(url));
                if (flags >= 0) {
                    builder.withFlags(flags);
                }
                intent.setOperation(builder.build());
                if (reqCode >= 0 && mContext instanceof Ability) {
                    ((Ability) mContext).startAbilityForResult(intent, reqCode);
                } else {
                    ((Ability) mContext).startAbility(intent);
                }
                if (mContext instanceof Ability && (inAnim >= 0 || outAnim >= 0)) {
                    ((Ability) mContext).setTransitionAnimation(inAnim, outAnim);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LogUtils.e(e);
        } finally {
            recycle();
        }
    }

    private void recycle() {
        mContext = null;
    }
}
