package com.qy.reader.common.widgets;

import com.qy.reader.common.ResourceTable;
import com.qy.reader.common.widgets.reader.ArrayItemProvider;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.ListContainer;
import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;

/**
 * Created by quezhongsang on 2018/1/13.
 */
public class ListDialog {

    CommonDialog materialDialog;

    public ListDialog(CommonDialog materialDialog) {
        this.materialDialog = materialDialog;
    }

    public static class Builder {
        CommonDialog realBuilder;
        Context context;

        public Builder(Context context) {
            this.context = context;
            realBuilder = new CommonDialog(context);
        }

        public ListDialog.Builder setTitle(CharSequence title) {
            realBuilder.setTitleText(String.valueOf(title));
            return this;
        }

        public ListDialog.Builder setList(final String[] titles, final OnItemClickListener onItemClickListener) {
            if (titles == null || titles.length <= 0)
                return this;
            final ArrayItemProvider<String> arrayAdapter = new ArrayItemProvider<>(this.context, ResourceTable.Layout_simple_list_item_1);
            for (String title : titles) {
                arrayAdapter.add(title + "");
            }

            ListContainer listView = new ListContainer(context);
            listView.setLayoutConfig(new ComponentContainer.LayoutConfig(
                    ComponentContainer.LayoutConfig.MATCH_PARENT,
                    ComponentContainer.LayoutConfig.MATCH_PARENT));
            float scale = DisplayManager.getInstance().getDefaultDisplay(context).get().getAttributes().scalDensity;
            int dpAsPixels = (int) (8 * scale + 0.5f);
            listView.setPadding(0, dpAsPixels, 0, dpAsPixels);
            listView.setItemProvider(arrayAdapter);

            listView.setItemClickedListener(new ListContainer.ItemClickedListener() {
                @Override
                public void onItemClicked(ListContainer listContainer, Component component, int position, long id) {
                    if (onItemClickListener != null) {
                        onItemClickListener.onItemClick(realBuilder, position, titles[position]);
                    }
                    realBuilder.destroy();
                }
            });

            realBuilder.setContentCustomComponent(listView);
            return this;
        }

        public ListDialog show() {
            realBuilder.show();
            return new ListDialog(realBuilder);
        }
    }

    public void show() {
        if (materialDialog != null) {
            materialDialog.show();
        }
    }


    public void dismiss() {
        if (materialDialog != null)
            materialDialog.destroy();
    }

    public interface OnItemClickListener {
        void onItemClick(CommonDialog materialDialog, int position, String content);
    }
}
