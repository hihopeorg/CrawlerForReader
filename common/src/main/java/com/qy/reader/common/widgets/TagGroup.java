package com.qy.reader.common.widgets;

import ohos.app.Context;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;

import com.qy.reader.common.utils.ScreenUtils;

/**
 * Created by yuyuhang on 2018/1/9.
 */
public class TagGroup extends ComponentContainer implements Component.EstimateSizeListener, ComponentContainer.ArrangeListener {

    private static final int SIDE_MARGIN = ScreenUtils.dpToPxInt(10);//布局边距
    private static final int VERTICAL_SPACING = ScreenUtils.dpToPxInt(6);
    private static final int HORIZONTAL_SPACING = ScreenUtils.dpToPxInt(8);

    private int verticalSpacing = VERTICAL_SPACING;
    private int horizontalSpacing = HORIZONTAL_SPACING;

    public TagGroup(Context context) {
        this(context, null);
    }

    public TagGroup(Context context, AttrSet attrSet) {
        this(context, attrSet, null);
    }

    public TagGroup(Context context, AttrSet attrSet, String styleName)  {
        super(context, attrSet, styleName);
        setEstimateSizeListener(this);
        setArrangeListener(this);
    }

    @Override
    public boolean onEstimateSize(int widthMeasureSpec, int heightMeasureSpec) {
        int x = SIDE_MARGIN;//横坐标
        int y = 0;//纵坐标
        int rows = 1;//总行数

        int widthSpecMode = EstimateSpec.getMode(widthMeasureSpec);

        int specWidth = EstimateSpec.getSize(widthMeasureSpec);
        int actualWidth = specWidth - SIDE_MARGIN * 2;//实际宽度;
        int childCount = getChildCount();

        // 处理宽高都为 wrap_content 的情况
        if (widthSpecMode == EstimateSpec.NOT_EXCEED) {
            for (int index = 0; index < childCount; index++) {
                Component child = getComponentAt(index);
                child.estimateSize(EstimateSpec.UNCONSTRAINT, EstimateSpec.UNCONSTRAINT);
                int width = child.getEstimatedWidth();
                int height = child.getEstimatedHeight();
                x += width + horizontalSpacing;
                if (x > actualWidth) {//换行
                    x = width;
                    rows++;
                }
                y = rows * (height + verticalSpacing);
            }
            if (rows == 1) {
                actualWidth = x;
            }
        } else {
            for (int index = 0; index < childCount; index++) {
                Component child = getComponentAt(index);
                child.estimateSize(EstimateSpec.UNCONSTRAINT, EstimateSpec.UNCONSTRAINT);
                int width = child.getEstimatedWidth();
                int height = child.getEstimatedHeight();
                x += width + horizontalSpacing;
                if (x > actualWidth) {//换行
                    x = width;
                    rows++;
                }
                y = rows * (height + verticalSpacing);
            }
        }
        setEstimatedSize(
                EstimateSpec.getSizeWithMode(actualWidth, EstimateSpec.PRECISE),
                EstimateSpec.getSizeWithMode(y, EstimateSpec.PRECISE));
        return true;
    }

    @Override
    public boolean onArrange(int l, int t, int r, int b) {
        int childCount = getChildCount();
        int autualWidth = r - l;
        int x = SIDE_MARGIN;// 横坐标开始
        int y = 0;//纵坐标开始
        int rows = 1;
        for (int i = 0; i < childCount; i++) {
            Component view = getComponentAt(i);
            int width = view.getEstimatedWidth();
            int height = view.getEstimatedHeight();
            x += width;
            if (i != 0)
                x += horizontalSpacing;
            if (x > autualWidth) {
                x = width + SIDE_MARGIN;
                rows++;
            }
            y = rows * (height + verticalSpacing);
            view.arrange(x - width, y - height, width, height);
        }
        return true;
    }

    public int getVerticalSpacing() {
        return verticalSpacing;
    }

    public int getHorizontalSpacing() {
        return horizontalSpacing;
    }

    public void setVerticalSpacing(int verticalSpacing) {
        this.verticalSpacing = verticalSpacing;
    }

    public void setHorizontalSpacing(int horizontalSpacing) {
        this.horizontalSpacing = horizontalSpacing;
    }
}