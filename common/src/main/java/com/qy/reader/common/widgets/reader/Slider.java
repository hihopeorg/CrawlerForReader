package com.qy.reader.common.widgets.reader;

import ohos.agp.render.Canvas;
import ohos.multimodalinput.event.TouchEvent;

/**
 * @author yuyh.
 * @date 17/3/24.
 */

public interface Slider {

    void bind(ReadView readView);

    void initListener(OnPageStateChangedListener listener);

    boolean onTouchEvent(TouchEvent event);

    void computeScroll();

    void onDraw(Canvas canvas);
}
