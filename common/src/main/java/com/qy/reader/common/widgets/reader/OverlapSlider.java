package com.qy.reader.common.widgets.reader;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.render.PixelMapHolder;

/**
 * @author yuyh.
 * @date 17/3/26.
 */
public class OverlapSlider extends BaseSlider {

    private Path mPath;

    private ShapeElement mShadowElement;
    private Paint paint = new Paint();

    public OverlapSlider() {
        mPath = new Path();

        RgbColor[] mBackShadowColors = new RgbColor[]{RgbColor.fromArgbInt(0xaa666666), RgbColor.fromArgbInt(0x666666)};

        mShadowElement = new ShapeElement();
        mShadowElement.setRgbColors(mBackShadowColors);
        mShadowElement.setGradientOrientation(ShapeElement.Orientation.LEFT_TO_RIGHT);
        mShadowElement.setShaderType(ShapeElement.LINEAR_GRADIENT_SHADER_TYPE);
    }

    /**
     * 绘制当前页
     *
     * @param canvas
     */
    void drawCurrentPageArea(Canvas canvas) {
        mPath.reset();

        canvas.save();

        PixelMapHolder holder = new PixelMapHolder(mReadView.getCurPageBitmap());

        // 手指滑动状态
        if (mMode == MODE_MOVE && mDirection != MOVE_NO_RESULT) { // 有效滑动（手指触摸滑动过程）
            mVelocityTracker.calculateCurrentVelocity(1000);
            if (mDirection == MOVE_TO_LEFT) {
                mPath.addRect(0, 0, mScreenWidth - distance, mScreenHeight, Path.Direction.CLOCK_WISE);
                canvas.clipPath(mPath, Canvas.ClipOp.INTERSECT);
                canvas.drawPixelMapHolder(holder, -distance, 0, paint);
            } else if (mDirection == MOVE_TO_RIGHT) {
                mPath.addRect(-distance, 0, mScreenWidth, mScreenHeight, Path.Direction.CLOCK_WISE);
                canvas.clipPath(mPath, Canvas.ClipOp.INTERSECT);
                canvas.drawPixelMapHolder(holder, 0, 0, paint);
            }
        } else if (autoScrollLeft != SCROLL_NONE) { // 自动左滑
            mPath.addRect(0, 0, -scrollDistance, mScreenHeight, Path.Direction.CLOCK_WISE);
            canvas.clipPath(mPath, Canvas.ClipOp.INTERSECT);
            canvas.drawPixelMapHolder(holder, -mScreenWidth - scrollDistance, 0, paint);
        } else if (autoScrollRight != SCROLL_NONE) { // 自动右滑
            mPath.addRect(-scrollDistance, 0, mScreenWidth, mScreenHeight, Path.Direction.CLOCK_WISE);
            canvas.clipPath(mPath, Canvas.ClipOp.INTERSECT);
            canvas.drawPixelMapHolder(holder, 0, 0, paint);
        } else { // 初始状态或无效滑动（eg: 先往左滑，又往右滑到初始状态，继续往右就不动）
            mPath.addRect(0, 0, mScreenWidth, mScreenHeight, Path.Direction.CLOCK_WISE);
            canvas.clipPath(mPath, Canvas.ClipOp.INTERSECT);
            canvas.drawPixelMapHolder(holder, 0, 0, paint);
        }
        canvas.restore();
    }

    /**
     * 绘制阴影
     *
     * @param canvas
     */
    void drawShadow(Canvas canvas) {
        canvas.save();
        if (mDirection != MOVE_NO_RESULT) {
            if (mDirection == MOVE_TO_LEFT) {
                mShadowElement.setBounds(mScreenWidth - distance, 0, mScreenWidth - distance + shadowSize, mScreenHeight);
            } else {
                mShadowElement.setBounds(-distance, 0, -distance + shadowSize, mScreenHeight);
            }
        } else {
            mShadowElement.setBounds(-scrollDistance, 0, -scrollDistance + shadowSize, mScreenHeight);
        }
        mShadowElement.drawToCanvas(canvas);
        canvas.restore();
    }

    /**
     * 绘制临时页（类似绘制当前页，只有在滑动过程需要绘制，否则只绘制当前页）
     *
     * @param canvas
     */
    void drawTempPageArea(Canvas canvas) {
        mPath.reset();

        canvas.save();

        if (mMode == MODE_MOVE && mDirection != MOVE_NO_RESULT) {
            mVelocityTracker.calculateCurrentVelocity(1000);
            if (mDirection == MOVE_TO_LEFT) {
                mPath.addRect(mScreenWidth - distance, 0, mScreenWidth, mScreenHeight, Path.Direction.CLOCK_WISE);
                canvas.clipPath(mPath, Canvas.ClipOp.INTERSECT);
                PixelMapHolder holder = new PixelMapHolder(mReadView.getNextPageBitmap());
                canvas.drawPixelMapHolder(holder, 0, 0, paint);
            } else if (mDirection == MOVE_TO_RIGHT) {
                mPath.addRect(-mScreenWidth - distance, 0, -distance, mScreenHeight, Path.Direction.CLOCK_WISE);
                canvas.clipPath(mPath, Canvas.ClipOp.INTERSECT);
                PixelMapHolder holder = new PixelMapHolder(mReadView.getPrePageBitmap());
                canvas.drawPixelMapHolder(holder, -mScreenWidth - distance, 0, paint);
            }
        } else {
            if (autoScrollLeft != SCROLL_NONE) {
                mPath.addRect(-scrollDistance, 0, mScreenWidth, mScreenHeight, Path.Direction.CLOCK_WISE);
                canvas.clipPath(mPath, Canvas.ClipOp.INTERSECT);
                PixelMapHolder holder = new PixelMapHolder(mReadView.getNextPageBitmap());
                canvas.drawPixelMapHolder(holder, 0, 0, paint);
            } else if (autoScrollRight != SCROLL_NONE) {
                mPath.addRect(-mScreenWidth - scrollDistance, 0, -scrollDistance, mScreenHeight, Path.Direction.CLOCK_WISE);
                canvas.clipPath(mPath, Canvas.ClipOp.INTERSECT);
                PixelMapHolder holder = new PixelMapHolder(mReadView.getPrePageBitmap());
                canvas.drawPixelMapHolder(holder, -mScreenWidth - scrollDistance, 0, paint);
            }
        }
        canvas.restore();
    }
}
