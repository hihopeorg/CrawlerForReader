package com.qy.reader.common.utils;

import ohos.agp.components.LayoutScatter;

import com.qy.reader.common.Global;
import com.qy.reader.common.ResourceTable;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;

/**
 * Toast工具类，防止多个Toast连续显示
 *
 * @author yuyh.
 * @date 17/2/10.
 */
public class ToastUtils {

    private static final int LENGTH_SHORT = 200;
    private static final int LENGTH_LONG = 300;

    private static ToastDialog mToast;

    /********************** 非连续弹出的Toast ***********************/
    public static void showSingleToast(int resId) { //R.string.**
        getSingleToast(resId, LENGTH_SHORT).show();
    }

    public static void showSingleToast(String text) {
        getSingleToast(text, LENGTH_SHORT).show();
    }

    public static void showSingleLongToast(int resId) {
        getSingleToast(resId, LENGTH_LONG).show();
    }

    public static void showSingleLongToast(String text) {
        getSingleToast(text, LENGTH_LONG).show();
    }

    /*********************** 连续弹出的Toast ************************/
    public static void showToast(int resId) {
        getToast(resId, LENGTH_SHORT).show();
    }

    public static void showToast(String text) {
        getToast(text, LENGTH_SHORT).show();
    }

    public static void showLongToast(int resId) {
        getToast(resId, LENGTH_LONG).show();
    }

    public static void showLongToast(String text) {
        getToast(text, LENGTH_LONG).show();
    }

    public static ToastDialog getSingleToast(int resId, int duration) { // 连续调用不会连续弹出，只是替换文本
        return getSingleToast(Global.getApplication().getString(resId), duration);
    }

    public static ToastDialog getSingleToast(String text, int duration) {
        if (mToast == null) {
            mToast = new ToastDialog(Global.getApplication());
            mToast.setComponent(LayoutScatter.getInstance(Global.getApplication()).parse(ResourceTable.Layout_common_toast_layout, null, false));
            mToast.setText(text);
            mToast.setAlignment(LayoutAlignment.BOTTOM);
            mToast.setOffset(0, ScreenUtils.dpToPxInt(100));
        } else {
            mToast.setText(text);
        }
        return mToast;
    }

    public static ToastDialog getToast(int resId, int duration) { // 连续调用会连续弹出
        return getToast(Global.getApplication().getString(resId), duration);
    }

    public static ToastDialog getToast(String text, int duration) {
        return new ToastDialog(Global.getApplication()).setText(text).setDuration(duration);
    }
}
