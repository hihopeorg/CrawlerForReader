package com.qy.reader.common.base;

import com.qy.reader.common.ResourceTable;
import ohos.aafwk.ability.fraction.FractionAbility;
import ohos.aafwk.content.Intent;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.StackLayout;
import ohos.agp.components.Text;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextTool;
import ohos.agp.window.service.WindowManager;

/**
 * Created by yuyuhang on 2018/1/8.
 */
public class BaseAbility extends FractionAbility implements IRoot {

    protected BaseAbility mContext;

    protected Text mTvBack;
    protected Text mTvTitle;

    protected Component statusBarView;

    @Override
    protected void onStart(Intent intent) {
        this.mContext = this;
        super.onStart(intent);

        if (enableStatusBarCompat()) {
//            statusBarView = StatusBarCompat.compat(this);
        }
    }

    protected void initToolbar() {
        mTvBack = (Text) findComponentById(ResourceTable.Id_toolbar_back);
        if (enableBackIcon()) {
            mTvBack.setVisibility(Component.VISIBLE);
            mTvBack.setClickedListener(new Component.ClickedListener() {
                @Override
                public void onClick(Component v) {
                    terminateAbility();
                }
            });
        } else {
            mTvBack.setVisibility(Component.HIDE);
        }

        mTvTitle = (Text) findComponentById(ResourceTable.Id_toolbar_title);
        if (mTvTitle != null) {
            String title = getToolbarTitle();
            if (!TextTool.isNullOrEmpty(title)) {
                mTvTitle.setText(title);
            }
        }
    }

    public boolean enableStatusBarCompat() {
        return true;
    }

    public boolean enableBackIcon() {
        return true;
    }

    public String getToolbarTitle() {
        return "";
    }

    protected void hideStatusBar() {
        WindowManager.LayoutConfig attrs = getWindow().getLayoutConfig().get();
        attrs.flags |= WindowManager.LayoutConfig.MARK_FULL_SCREEN;
        getWindow().setLayoutConfig(attrs);
        if (statusBarView != null) {
            ShapeElement bg = new ShapeElement();
            bg.setRgbColor(RgbColor.fromArgbInt(Color.TRANSPARENT.getValue()));
            statusBarView.setBackground(bg);
        }
    }

    protected void showStatusBar() {
        WindowManager.LayoutConfig attrs = getWindow().getLayoutConfig().get();
        attrs.flags &= ~WindowManager.LayoutConfig.MARK_FULL_SCREEN;
        getWindow().setLayoutConfig(attrs);
        if (statusBarView != null) {
            ShapeElement bg = new ShapeElement();
            bg.setRgbColor(RgbColor.fromArgbInt(getColor(ResourceTable.Color_colorPrimaryDark)));
            statusBarView.setBackground(bg);
        }
    }

    protected boolean isVisible(Component view) {
        return view.getVisibility() == Component.VISIBLE;
    }

    protected void gone(final Component... views) {
        if (views != null && views.length > 0) {
            for (Component view : views) {
                if (view != null) {
                    view.setVisibility(Component.HIDE);
                }
            }
        }
    }

    protected void invisible(final Component... views) {
        if (views != null && views.length > 0) {
            for (Component view : views) {
                if (view != null) {
                    view.setVisibility(Component.INVISIBLE);
                }
            }
        }
    }

    protected void visible(final Component... views) {
        if (views != null && views.length > 0) {
            for (Component view : views) {
                if (view != null) {
                    view.setVisibility(Component.VISIBLE);
                }
            }
        }
    }

    private StackLayout root = null;

    @Override
    public void setUIContent(ComponentContainer componentContainer) {
        root = new StackLayout(this);
        ComponentContainer.LayoutConfig layoutConfig = new ComponentContainer.LayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_PARENT, ComponentContainer.LayoutConfig.MATCH_PARENT);
        root.setLayoutConfig(layoutConfig);
        root.addComponent(componentContainer);
        super.setUIContent(root);
    }

    @Override
    public Component getRootComponent() {
        return root;
    }
}
