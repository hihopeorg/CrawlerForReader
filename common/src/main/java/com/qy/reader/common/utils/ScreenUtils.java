package com.qy.reader.common.utils;

import com.qy.reader.common.Global;
import ohos.agp.components.AttrHelper;
import ohos.agp.utils.Point;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.app.Context;
import ohos.bundle.AbilityInfo;

/**
 * Screen utils
 * <p>
 * Created by yuyuhang on 2018/1/8.
 */
public class ScreenUtils {

    public static int getScreenWidth() {
        return DisplayManager.getInstance().getDefaultDisplay(Global.getApplication()).get().getAttributes().width;
    }

    public static int getScreenHeight() {
        return DisplayManager.getInstance().getDefaultDisplay(Global.getApplication()).get().getAttributes().height;
    }

    public static float dpToPx(float dp) {
        return dp * DisplayManager.getInstance().getDefaultDisplay(Global.getApplication()).get().getAttributes().scalDensity;
    }

    public static int dpToPxInt(float dp) {
        return (int) (dpToPx(dp) + 0.5f);
    }

    public static float pxToDp(float px) {
        return px / DisplayManager.getInstance().getDefaultDisplay(Global.getApplication()).get().getAttributes().scalDensity;
    }

    public static int pxToDpInt(float px) {
        return (int) (pxToDp(px) + 0.5f);
    }

    public static float pxToSp(float pxValue) {
        return pxValue / DisplayManager.getInstance().getDefaultDisplay(Global.getApplication()).get().getAttributes().scalDensity;
    }

    public static float spToPx(float spValue) {
        return spValue * DisplayManager.getInstance().getDefaultDisplay(Global.getApplication()).get().getAttributes().scalDensity;
    }

    public static int getStatusBarHeight(Context context) {
        Display display = DisplayManager.getInstance().getDefaultDisplay(context).get();

        Point realSize = new Point();
        display.getRealSize(realSize);

        Point size = new Point();
        display.getSize(size);

        return (int)(realSize.getPointY() - size.getPointY());
    }

    /**
     * Get pixels from dps
     *
     * @param dp
     * @return pixels
     */
    public static int getIntPixels(final int dp) {
        return AttrHelper.vp2px(dp, Global.getApplication());
    }

    public static final boolean isLandscape(Context context) {
        return context.getDisplayOrientation() == AbilityInfo.DisplayOrientation.LANDSCAPE.ordinal();
    }

    public static final boolean isPortrait(Context context) {
        return context.getDisplayOrientation() == AbilityInfo.DisplayOrientation.PORTRAIT.ordinal();
    }
}
