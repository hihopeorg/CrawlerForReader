/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.qy.reader.common.utils;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

public class Log {
    private static final int DOMAIN = 0x1234;
    private static final String TAG = "zhg";

    public static final int VERBOSE = HiLog.DEBUG;
    public static final int DEBUG = HiLog.DEBUG;
    public static final int INFO = HiLog.INFO;
    public static final int WARN = HiLog.WARN;
    public static final int ERROR = HiLog.ERROR;
    public static final int ASSERT = HiLog.FATAL;

    public static void d(String tag, String msg) {
        HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, DOMAIN, tag);
        HiLog.debug(label, msg);
    }

    public static void i(String tag, String msg) {
        HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, DOMAIN, tag);
        HiLog.info(label, msg);
    }

    public static void v(String tag, String msg) {
        HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, DOMAIN, tag);
        HiLog.info(label, msg);
    }

    public static void w(String tag, String msg) {
        HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, DOMAIN, tag);
        HiLog.warn(label, msg);
    }

    public static void w(String tag, String msg, Throwable throwable) {
        HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, DOMAIN, tag);
        HiLog.warn(label, msg);
    }

    public static void e(String tag, String msg) {
        HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, DOMAIN, tag);
        HiLog.error(label, msg);
    }

    public static void e(String tag, Throwable throwable) {
        HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, DOMAIN, tag);
        HiLog.error(label, getStackTrace(throwable));
    }

    private static String getStackTrace(Throwable throwable) {
        StringBuilder sb = new StringBuilder();
        StackTraceElement[] stackTraceElements = throwable.getStackTrace();
        if(stackTraceElements!=null) {
            for(StackTraceElement element : stackTraceElements) {
                sb.append(element).append("\n");
            }
        }
        return sb.toString();
    }

    public static void f(String tag, String msg) {
        HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, DOMAIN, tag);
        HiLog.fatal(label, msg);
    }

    public static void i(String msg) {
        HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, DOMAIN, TAG);
        HiLog.info(label, msg);
    }

    public static void e(String msg) {
        HiLogLabel label = new HiLogLabel(HiLog.LOG_APP, DOMAIN, TAG);
        HiLog.error(label, msg);
    }

    public static void println(int type, String tag, String msg) {
        switch (type) {
            case HiLog.WARN:
                w(tag, msg);
                break;
            case HiLog.INFO:
                i(tag, msg);
                break;
            case HiLog.DEBUG:
                d(tag, msg);
                break;
            case HiLog.ERROR:
                e(tag, msg);
                break;
            case HiLog.FATAL:
                f(tag, msg);
                break;
        }
    }
}
