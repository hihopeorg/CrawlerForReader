package com.qy.reader.common.widgets.reader;

import ohos.agp.render.Texture;
import ohos.app.Context;
import ohos.media.image.PixelMap;
import ohos.agp.render.Canvas;
import ohos.agp.components.AttrSet;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;
import ohos.multimodalinput.event.TouchEvent;
import ohos.agp.components.Component;

import com.qy.reader.common.entity.chapter.Chapter;
import com.qy.reader.common.entity.source.SourceID;
import com.qy.reader.common.utils.BitmapUtils;
import com.qy.reader.common.utils.ScreenUtils;
import com.qy.reader.common.widgets.reader.annotation.ChapterType;
import com.qy.reader.common.widgets.reader.annotation.DrawPageType;
import com.qy.reader.common.widgets.reader.annotation.SlideMode;

import java.util.List;

/**
 * Created by yuyuhang on 2018/1/11.
 */
public class ReadView extends Component implements Component.DrawTask, Component.TouchEventListener, Component.BindStateChangedListener {

    protected int mScreenWidth;
    protected int mScreenHeight;

    protected PixelMap mCurPageBitmap, mNextPageBitmap, mBgBitmap;
    protected Canvas mCurrentPageCanvas, mNextPageCanvas, mBgCanvas;

    private int slideMode = SlideMode.FOLLOW;
    private BaseSlider mSlider;

    private PageFactory mFactory = null;

    private OnPageStateChangedListener listener;

    private boolean isInit = false;

    public ReadView(Context context) {
        this(context, null);
    }

    public ReadView(Context context, AttrSet attrs) {
        this(context, attrs, 0);
    }

    public ReadView(Context context, AttrSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mScreenWidth = ScreenUtils.getScreenWidth();
        mScreenHeight = ScreenUtils.getScreenHeight();

        PixelMap.InitializationOptions initializationOptions = new PixelMap.InitializationOptions();
        initializationOptions.pixelFormat = PixelFormat.ARGB_8888;
        initializationOptions.size = new Size(mScreenWidth, mScreenHeight);
        initializationOptions.editable = true;
        mCurPageBitmap = PixelMap.create(initializationOptions);
        mNextPageBitmap = PixelMap.create(initializationOptions);
        mCurrentPageCanvas = new Canvas(new Texture(mCurPageBitmap));
        mNextPageCanvas = new Canvas(new Texture(mNextPageBitmap));

        setClickable(true);

        setSlideMode(slideMode);

        mFactory = new PageFactory();

        addDrawTask(this);
        setTouchEventListener(this);
        setBindStateChangedListener(this);
    }

    /**
     * 切换横竖屏，由于宽高发生变化，需要重新计算画布大小
     */
    public void initScreenSize() {
        mScreenWidth = ScreenUtils.getScreenWidth();
        mScreenHeight = ScreenUtils.getScreenHeight();

        mCurPageBitmap = BitmapUtils.scaleBitmap(mCurPageBitmap, mScreenWidth, mScreenHeight);
        mNextPageBitmap = BitmapUtils.scaleBitmap(mNextPageBitmap, mScreenWidth, mScreenHeight);

        mCurrentPageCanvas = new Canvas(new Texture(mCurPageBitmap));
        mNextPageCanvas = new Canvas(new Texture(mNextPageBitmap));

        mSlider.initScreenSize();
        mFactory.initScreenSize();
        mFactory.pages(ChapterType.CURRENT);

        invalidate();
    }

    public void initChapterList(@SourceID int sourceId, String bookNum, List<Chapter> list, int chapterIndex, int pageIndex) {
        if (!isInit || mFactory.getBufferLen() < 1) { // 未初始化或读取章节失败，则重新初始化
            isInit = true;

            mFactory.initBook(sourceId, bookNum, list, chapterIndex, pageIndex);
            mFactory.openBook(chapterIndex, ChapterType.CURRENT);
        }

        invalidate();
    }

    public boolean isInit() {
        return isInit;
    }

    public PageFactory getPageFactory() {
        return mFactory;
    }

    public PixelMap getCurPageBitmap() {
        if (!mSlider.isPreparedCurPage) {
            mSlider.isPreparedCurPage = true;
            mFactory.drawCurPage(mCurrentPageCanvas, DrawPageType.DRAW_CUR_PAGE);
        }
        return mCurPageBitmap;
    }

    public PixelMap getNextPageBitmap() {
        if (!mSlider.isPreparedTempPage) {
            mSlider.isPreparedTempPage = true;
            mFactory.drawCurPage(mNextPageCanvas, DrawPageType.DRAW_NEXT_PAGE);
        }
        return mNextPageBitmap;
    }

    public PixelMap getPrePageBitmap() {
        if (!mSlider.isPreparedTempPage) {
            mSlider.isPreparedTempPage = true;
            mFactory.drawCurPage(mNextPageCanvas, DrawPageType.DRAW_PRE_PAGE);
        }
        return mNextPageBitmap;
    }

    public void nextPage() {
        mFactory.nextPage();
        invalidate();
    }

    public void prePage() {
        mFactory.prePage();
        invalidate();
    }

    public void preChapter() {
        mFactory.preChapter();
        invalidate();
    }

    public void nextChapter() {
        mFactory.nextChapter();
        invalidate();
    }

    public void jumpChapter(int chapter) {
        mFactory.jumpChapter(chapter);
        invalidate();
    }

    public void setOnPageStateChangedListener(OnPageStateChangedListener listener) {
        this.listener = listener;
        mSlider.initListener(listener);
        mFactory.initListener(listener);
    }

    /**
     * 设置滑动模式
     *
     * @param mode
     */
    public void setSlideMode(@SlideMode int mode) {
        if (mode == SlideMode.FOLLOW) {
            mSlider = new FollowSlider();
        } else {
            mSlider = new OverlapSlider();
        }
        mSlider.bind(this);
        mSlider.initListener(listener);
    }

    public void computeScroll() {
        mSlider.computeScroll();
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        mSlider.onDraw(canvas);
        computeScroll();
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        return mSlider.onTouchEvent(event);
    }

    @Override
    public void invalidate() {
        mSlider.isPreparedCurPage = false;
        mSlider.isPreparedTempPage = false;
        super.invalidate();
    }

    @Override
    public void onComponentBoundToWindow(Component component) {
    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
        recycler();
    }

    public void recycler() {

        mCurrentPageCanvas = null;
        mNextPageCanvas = null;
        mBgCanvas = null;

        BitmapUtils.recycler(mBgBitmap);
        BitmapUtils.recycler(mCurPageBitmap);
        BitmapUtils.recycler(mNextPageBitmap);

        mFactory.recycler();
        mFactory = null;
    }
}
