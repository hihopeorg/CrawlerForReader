package com.qy.reader.common.base;

import com.qy.reader.common.ResourceTable;
import com.qy.reader.common.utils.Nav;
import ohos.aafwk.ability.fraction.Fraction;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yuyuhang on 2018/1/9.
 */
public abstract class BaseTabAbility extends BaseAbility implements Component.ClickedListener {

    protected static final int HOME_INDEX = 0;
    protected static final int SEARCH_INDEX = 1;
    protected static final int DISCOVER_INDEX = 2;
    protected static final int MINE_INDEX = 3;

    protected Fraction mFraction;

    protected Text mTvHome, mTvSearch, mTvDiscover, mTvMine;

    private List<TAB> mTabList = new ArrayList<>();

    private static int currentIndex = -1;

    protected int pageIndex = -1;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);

        initContentView();
    }

    private void initContentView() {
        pageIndex = getCurrentIndex();
        mFraction = fractionInstance();

        setUIContent(ResourceTable.Layout_ability_base_tab);

        mTvHome = (Text) findComponentById(ResourceTable.Id_tv_tab_home);
        mTvSearch = (Text) findComponentById(ResourceTable.Id_tv_tab_search);
        mTvDiscover = (Text) findComponentById(ResourceTable.Id_tv_tab_discover);
        mTvMine = (Text) findComponentById(ResourceTable.Id_tv_tab_mine);

        mTvHome.setClickedListener(this);
        mTvSearch.setClickedListener(this);
        mTvDiscover.setClickedListener(this);
        mTvMine.setClickedListener(this);

        mTabList.clear();
        mTabList.add(new TAB(mTvHome, "qyreader://home"));
        mTabList.add(new TAB(mTvSearch, "qyreader://search"));
        mTabList.add(new TAB(mTvDiscover, "qyreader://discover"));
        mTabList.add(new TAB(mTvMine, "qyreader://mine"));

        getFractionManager().startFractionScheduler().replace(ResourceTable.Id_fl_tab_content, mFraction).submit();
    }

    protected abstract int getCurrentIndex();

    protected abstract Fraction fractionInstance();

    @Override
    protected void onActive() {
        super.onActive();
        currentIndex = pageIndex;
        for (int i = 0; i < mTabList.size(); i++) {
            mTabList.get(i).tabView.setSelected(i == currentIndex);
        }
    }

    @Override
    public void onClick(Component v) {
        for (int i = 0; i < mTabList.size(); i++) {
            TAB tab = mTabList.get(i);
            if (tab.tabView == v) {
                if (currentIndex != i) {
                    Nav.from(this)
                            .setFlags(Intent.FLAG_ABILITY_CLEAR_MISSION)
                            .overridePendingTransition(0, 0)
                            .start(tab.url);
                }
            }
        }
    }

    public static class TAB {

        Component tabView;

        String url;

        public TAB(Component tabView, String url) {
            this.tabView = tabView;
            this.url = url;
        }
    }
}
