package com.qy.reader.common.utils;

import com.qy.reader.common.Global;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.render.Canvas;
import ohos.agp.render.Texture;
import ohos.media.image.ImagePacker;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.PixelFormat;
import ohos.media.image.common.Size;
import ohos.os.ProcessManager;
import ohos.utils.LightweightMap;
import ohos.utils.Parcel;
import ohos.utils.Sequenceable;
import ohos.utils.zson.ZSONArray;
import ohos.utils.zson.ZSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.io.Serializable;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;


public final class CacheUtils {

    private static final long DEFAULT_MAX_SIZE = Long.MAX_VALUE;
    private static final int DEFAULT_MAX_COUNT = Integer.MAX_VALUE;

    public static final int SEC = 1;
    public static final int MIN = 60;
    public static final int HOUR = 3600;
    public static final int DAY = 86400;

    private static final LightweightMap<String, CacheUtils> CACHE_MAP = new LightweightMap<>();
    private CacheManager mCacheManager;

    /**
     * 获取缓存实例
     * <p>在 /data/data/com.xxx.xxx/cache/cacheUtils 目录</p>
     * <p>缓存尺寸不限</p>
     * <p>缓存个数不限</p>
     *
     * @return {@link CacheUtils}
     */
    public static CacheUtils getInstance() {
        return getInstance("cache", DEFAULT_MAX_SIZE, DEFAULT_MAX_COUNT);
    }

    /**
     * 获取缓存实例
     * <p>在 /data/data/com.xxx.xxx/cache/cacheName 目录</p>
     * <p>缓存尺寸不限</p>
     * <p>缓存个数不限</p>
     *
     * @param cacheName 缓存目录名
     * @return {@link CacheUtils}
     */
    public static CacheUtils getInstance(final String cacheName) {
        return getInstance(cacheName, DEFAULT_MAX_SIZE, DEFAULT_MAX_COUNT);
    }

    /**
     * 获取缓存实例
     * <p>在 /data/data/com.xxx.xxx/cache/cacheUtils目录 </p>
     *
     * @param maxSize  最大缓存尺寸，单位字节
     * @param maxCount 最大缓存个数
     * @return {@link CacheUtils}
     */
    public static CacheUtils getInstance(final long maxSize, final int maxCount) {
        return getInstance("", maxSize, maxCount);
    }

    /**
     * 获取缓存实例
     * <p>在 /data/data/com.xxx.xxx/cache/cacheName 目录</p>
     *
     * @param cacheName 缓存目录名
     * @param maxSize   最大缓存尺寸，单位字节
     * @param maxCount  最大缓存个数
     * @return {@link CacheUtils}
     */
    public static CacheUtils getInstance(String cacheName, final long maxSize, final int maxCount) {
        if (isSpace(cacheName)) cacheName = "cacheUtils";
        File file = new File(Global.getApplication().getCacheDir(), cacheName);
        return getInstance(file, maxSize, maxCount);
    }

    /**
     * 获取缓存实例
     * <p>在 cacheDir 目录</p>
     * <p>缓存尺寸不限</p>
     * <p>缓存个数不限</p>
     *
     * @param cacheDir 缓存目录
     * @return {@link CacheUtils}
     */
    public static CacheUtils getInstance(final File cacheDir) {
        return getInstance(cacheDir, DEFAULT_MAX_SIZE, DEFAULT_MAX_COUNT);
    }

    /**
     * 获取缓存实例
     * <p>在 cacheDir 目录</p>
     *
     * @param cacheDir 缓存目录
     * @param maxSize  最大缓存尺寸，单位字节
     * @param maxCount 最大缓存个数
     * @return {@link CacheUtils}
     */
    public static CacheUtils getInstance(final File cacheDir, final long maxSize, final int maxCount) {
        final String cacheKey = cacheDir.getAbsoluteFile() + "_" + ProcessManager.getPid();
        CacheUtils cache = CACHE_MAP.get(cacheKey);
        if (cache == null) {
            cache = new CacheUtils(cacheDir, maxSize, maxCount);
            CACHE_MAP.put(cacheKey, cache);
        }
        return cache;
    }

    private CacheUtils(final File cacheDir, final long maxSize, final int maxCount) {
        if (!cacheDir.exists() && !cacheDir.mkdirs()) {
            throw new RuntimeException("can't make dirs in " + cacheDir.getAbsolutePath());
        }
        mCacheManager = new CacheManager(cacheDir, maxSize, maxCount);
    }

    ///////////////////////////////////////////////////////////////////////////
    // bytes 读写
    ///////////////////////////////////////////////////////////////////////////

    /**
     * 缓存中写入字节数组
     *
     * @param key   键
     * @param value 值
     */
    public void put(final String key, final byte[] value) {
        put(key, value, -1);
    }

    /**
     * 缓存中写入字节数组
     *
     * @param key      键
     * @param value    值
     * @param saveTime 保存时长，单位：秒
     */
    public void put(final String key, byte[] value, final int saveTime) {
        if (value.length <= 0) return;
        if (saveTime >= 0) value = CacheHelper.newByteArrayWithTime(saveTime, value);
        File file = mCacheManager.getFileBeforePut(key);
        CacheHelper.writeFileFromBytes(file, value);
        mCacheManager.updateModify(file);
        mCacheManager.put(file);

    }

    /**
     * 缓存中读取字节数组
     *
     * @param key 键
     * @return 存在且没过期返回对应值，否则返回{@code null}
     */
    public byte[] getBytes(final String key) {
        return getBytes(key, null);
    }

    /**
     * 缓存中读取字节数组
     *
     * @param key          键
     * @param defaultValue 默认值
     * @return 存在且没过期返回对应值，否则返回默认值{@code defaultValue}
     */
    public byte[] getBytes(final String key, final byte[] defaultValue) {
        final File file = mCacheManager.getFileIfExists(key);
        if (file == null) return defaultValue;
        byte[] data = CacheHelper.readFile2Bytes(file);
        if (CacheHelper.isDue(data)) {
            mCacheManager.removeByKey(key);
            return defaultValue;
        }
        mCacheManager.updateModify(file);
        return CacheHelper.getDataWithoutDueTime(data);
    }

    ///////////////////////////////////////////////////////////////////////////
    // String 读写
    ///////////////////////////////////////////////////////////////////////////

    /**
     * 缓存中写入 String
     *
     * @param key   键
     * @param value 值
     */
    public void put(final String key, final String value) {
        put(key, value, -1);
    }

    /**
     * 缓存中写入 String
     *
     * @param key      键
     * @param value    值
     * @param saveTime 保存时长，单位：秒
     */
    public void put(final String key, final String value, final int saveTime) {
        put(key, CacheHelper.string2Bytes(value), saveTime);
    }

    /**
     * 缓存中读取 String
     *
     * @param key 键
     * @return 存在且没过期返回对应值，否则返回{@code null}
     */
    public String getString(final String key) {
        return getString(key, null);
    }

    /**
     * 缓存中读取 String
     *
     * @param key          键
     * @param defaultValue 默认值
     * @return 存在且没过期返回对应值，否则返回默认值{@code defaultValue}
     */
    public String getString(final String key, final String defaultValue) {
        byte[] bytes = getBytes(key);
        if (bytes == null) return defaultValue;
        return CacheHelper.bytes2String(bytes);
    }

    ///////////////////////////////////////////////////////////////////////////
    // ZSONObject 读写
    ///////////////////////////////////////////////////////////////////////////

    /**
     * 缓存中写入 ZSONObject
     *
     * @param key   键
     * @param value 值
     */
    public void put(final String key, final ZSONObject value) {
        put(key, value, -1);
    }

    /**
     * 缓存中写入 ZSONObject
     *
     * @param key      键
     * @param value    值
     * @param saveTime 保存时长，单位：秒
     */
    public void put(final String key, final ZSONObject value, final int saveTime) {
        put(key, CacheHelper.ZSONObject2Bytes(value), saveTime);
    }

    /**
     * 缓存中读取 ZSONObject
     *
     * @param key 键
     * @return 存在且没过期返回对应值，否则返回{@code null}
     */
    public ZSONObject getZSONObject(final String key) {
        return getZSONObject(key, null);
    }

    /**
     * 缓存中读取 ZSONObject
     *
     * @param key          键
     * @param defaultValue 默认值
     * @return 存在且没过期返回对应值，否则返回默认值{@code defaultValue}
     */
    public ZSONObject getZSONObject(final String key, final ZSONObject defaultValue) {
        byte[] bytes = getBytes(key);
        if (bytes == null) return defaultValue;
        return CacheHelper.bytes2ZSONObject(bytes);
    }


    ///////////////////////////////////////////////////////////////////////////
    // ZSONArray 读写
    ///////////////////////////////////////////////////////////////////////////

    /**
     * 缓存中写入 ZSONArray
     *
     * @param key   键
     * @param value 值
     */
    public void put(final String key, final ZSONArray value) {
        put(key, value, -1);
    }

    /**
     * 缓存中写入 ZSONArray
     *
     * @param key      键
     * @param value    值
     * @param saveTime 保存时长，单位：秒
     */
    public void put(final String key, final ZSONArray value, final int saveTime) {
        put(key, CacheHelper.jsonArray2Bytes(value), saveTime);
    }

    /**
     * 缓存中读取 ZSONArray
     *
     * @param key 键
     * @return 存在且没过期返回对应值，否则返回{@code null}
     */
    public ZSONArray getJSONArray(final String key) {
        return getJSONArray(key, null);
    }

    /**
     * 缓存中读取 ZSONArray
     *
     * @param key          键
     * @param defaultValue 默认值
     * @return 存在且没过期返回对应值，否则返回默认值{@code defaultValue}
     */
    public ZSONArray getJSONArray(final String key, final ZSONArray defaultValue) {
        byte[] bytes = getBytes(key);
        if (bytes == null) return defaultValue;
        return CacheHelper.bytes2JSONArray(bytes);
    }


    ///////////////////////////////////////////////////////////////////////////
    // PixelMap 读写
    ///////////////////////////////////////////////////////////////////////////

    /**
     * 缓存中写入 PixelMap
     *
     * @param key   键
     * @param value 值
     */
    public void put(final String key, final PixelMap value) {
        put(key, value, -1);
    }

    /**
     * 缓存中写入 PixelMap
     *
     * @param key      键
     * @param value    值
     * @param saveTime 保存时长，单位：秒
     */
    public void put(final String key, final PixelMap value, final int saveTime) {
        put(key, CacheHelper.bitmap2Bytes(value), saveTime);
    }

    /**
     * 缓存中读取 PixelMap
     *
     * @param key 键
     * @return 存在且没过期返回对应值，否则返回{@code null}
     */
    public PixelMap getBitmap(final String key) {
        return getBitmap(key, null);
    }

    /**
     * 缓存中读取 PixelMap
     *
     * @param key          键
     * @param defaultValue 默认值
     * @return 存在且没过期返回对应值，否则返回默认值{@code defaultValue}
     */
    public PixelMap getBitmap(final String key, final PixelMap defaultValue) {
        byte[] bytes = getBytes(key);
        if (bytes == null) return defaultValue;
        return CacheHelper.bytes2Bitmap(bytes);
    }

    ///////////////////////////////////////////////////////////////////////////
    // Element 读写
    ///////////////////////////////////////////////////////////////////////////

    /**
     * 缓存中写入 Element
     *
     * @param key   键
     * @param value 值
     */
    public void put(final String key, final Element value) {
        put(key, CacheHelper.drawable2Bytes(value));
    }

    /**
     * 缓存中写入 Element
     *
     * @param key      键
     * @param value    值
     * @param saveTime 保存时长，单位：秒
     */
    public void put(final String key, final Element value, final int saveTime) {
        put(key, CacheHelper.drawable2Bytes(value), saveTime);
    }

    /**
     * 缓存中读取 Element
     *
     * @param key 键
     * @return 存在且没过期返回对应值，否则返回{@code null}
     */
    public Element getDrawable(final String key) {
        return getDrawable(key, null);
    }

    /**
     * 缓存中读取 Element
     *
     * @param key          键
     * @param defaultValue 默认值
     * @return 存在且没过期返回对应值，否则返回默认值{@code defaultValue}
     */
    public Element getDrawable(final String key, final Element defaultValue) {
        byte[] bytes = getBytes(key);
        if (bytes == null) return defaultValue;
        return CacheHelper.bytes2Drawable(bytes);
    }

    ///////////////////////////////////////////////////////////////////////////
    // Sequenceable 读写
    ///////////////////////////////////////////////////////////////////////////

    /**
     * 缓存中写入 Sequenceable
     *
     * @param key   键
     * @param value 值
     */
    public void put(final String key, final Sequenceable value) {
        put(key, value, -1);
    }

    /**
     * 缓存中写入 Sequenceable
     *
     * @param key      键
     * @param value    值
     * @param saveTime 保存时长，单位：秒
     */
    public void put(final String key, final Sequenceable value, final int saveTime) {
        put(key, CacheHelper.parcelable2Bytes(value), saveTime);
    }

    /**
     * 缓存中读取 Sequenceable
     *
     * @param key     键
     * @param creator 建造器
     * @return 存在且没过期返回对应值，否则返回{@code null}
     */
    public <T> T getParcelable(final String key, final Sequenceable.Producer<T> creator) {
        return getParcelable(key, creator, null);
    }

    /**
     * 缓存中读取 Sequenceable
     *
     * @param key          键
     * @param creator      建造器
     * @param defaultValue 默认值
     * @return 存在且没过期返回对应值，否则返回默认值{@code defaultValue}
     */
    public <T> T getParcelable(final String key, final Sequenceable.Producer<T> creator, final T defaultValue) {
        byte[] bytes = getBytes(key);
        if (bytes == null) return defaultValue;
        return CacheHelper.bytes2Parcelable(bytes, creator);
    }

    ///////////////////////////////////////////////////////////////////////////
    // Serializable 读写
    ///////////////////////////////////////////////////////////////////////////

    /**
     * 缓存中写入 Serializable
     *
     * @param key   键
     * @param value 值
     */
    public void put(final String key, final Serializable value) {
        put(key, value, -1);
    }

    /**
     * 缓存中写入 Serializable
     *
     * @param key      键
     * @param value    值
     * @param saveTime 保存时长，单位：秒
     */
    public void put(final String key, final Serializable value, final int saveTime) {
        put(key, CacheHelper.serializable2Bytes(value), saveTime);
    }

    /**
     * 缓存中读取 Serializable
     *
     * @param key 键
     * @return 存在且没过期返回对应值，否则返回{@code null}
     */
    public Object getSerializable(final String key) {
        return getSerializable(key, null);
    }

    /**
     * 缓存中读取 Serializable
     *
     * @param key          键
     * @param defaultValue 默认值
     * @return 存在且没过期返回对应值，否则返回默认值{@code defaultValue}
     */
    public Object getSerializable(final String key, final Object defaultValue) {
        byte[] bytes = getBytes(key);
        if (bytes == null) return defaultValue;
        return CacheHelper.bytes2Object(getBytes(key));
    }

    /**
     * 获取缓存大小
     * <p>单位：字节</p>
     * <p>调用了 Thread.join()，需异步调用，否则可能主线程会卡顿</p>
     *
     * @return 缓存大小
     */
    public long getCacheSize() {
        return mCacheManager.getCacheSize();
    }

    /**
     * 获取缓存个数
     * <p>调用了 Thread.join()，需异步调用，否则可能主线程会卡顿</p>
     *
     * @return 缓存个数
     */
    public int getCacheCount() {
        return mCacheManager.getCacheCount();
    }

    /**
     * 根据键值移除缓存
     *
     * @param key 键
     * @return {@code true}: 移除成功<br>{@code false}: 移除失败
     */
    public boolean remove(final String key) {
        return mCacheManager.removeByKey(key);
    }

    /**
     * 清除所有缓存
     *
     * @return {@code true}: 清除成功<br>{@code false}: 清除失败
     */
    public boolean clear() {
        return mCacheManager.clear();
    }

    private class CacheManager {
        private final AtomicLong cacheSize;
        private final AtomicInteger cacheCount;
        private final long sizeLimit;
        private final int countLimit;
        private final Map<File, Long> lastUsageDates = Collections.synchronizedMap(new HashMap<File, Long>());
        private final File cacheDir;
        private final Thread mThread;

        private CacheManager(final File cacheDir, final long sizeLimit, final int countLimit) {
            this.cacheDir = cacheDir;
            this.sizeLimit = sizeLimit;
            this.countLimit = countLimit;
            cacheSize = new AtomicLong();
            cacheCount = new AtomicInteger();
            mThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    int size = 0;
                    int count = 0;
                    final File[] cachedFiles = cacheDir.listFiles();
                    if (cachedFiles != null) {
                        for (File cachedFile : cachedFiles) {
                            size += cachedFile.length();
                            count += 1;
                            lastUsageDates.put(cachedFile, cachedFile.lastModified());
                        }
                        cacheSize.getAndAdd(size);
                        cacheCount.getAndAdd(count);
                    }
                }
            });
            mThread.start();
        }

        private long getCacheSize() {
            try {
                mThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return cacheSize.get();
        }

        private int getCacheCount() {
            try {
                mThread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return cacheCount.get();
        }

        private File getFileBeforePut(final String key) {
            File file = new File(cacheDir, String.valueOf(key.hashCode()));
            if (file.exists()) {
                cacheCount.addAndGet(-1);
                cacheSize.addAndGet(-file.length());
            }
            return file;
        }

        private File getFileIfExists(final String key) {
            File file = new File(cacheDir, String.valueOf(key.hashCode()));
            if (!file.exists()) return null;
            return file;
        }

        private void put(final File file) {
            cacheCount.addAndGet(1);
            cacheSize.addAndGet(file.length());
            while (cacheCount.get() > countLimit || cacheSize.get() > sizeLimit) {
                cacheSize.addAndGet(-removeOldest());
                cacheCount.addAndGet(-1);
            }
        }

        private void updateModify(final File file) {
            Long millis = System.currentTimeMillis();
            file.setLastModified(millis);
            lastUsageDates.put(file, millis);
        }

        private boolean removeByKey(final String key) {
            File file = getFileIfExists(key);
            if (file == null) return true;
            if (!file.delete()) return false;
            cacheSize.addAndGet(-file.length());
            cacheCount.addAndGet(-1);
            lastUsageDates.remove(file);
            return true;
        }

        private boolean clear() {
            File[] files = cacheDir.listFiles();
            if (files == null || files.length <= 0) return true;
            boolean flag = true;
            for (File file : files) {
                if (!file.delete()) {
                    flag = false;
                    continue;
                }
                cacheSize.addAndGet(-file.length());
                cacheCount.addAndGet(-1);
                lastUsageDates.remove(file);
            }
            if (flag) {
                lastUsageDates.clear();
                cacheSize.set(0);
                cacheCount.set(0);
            }
            return flag;
        }

        /**
         * 移除旧的文件
         *
         * @return 移除的字节数
         */
        private long removeOldest() {
            if (lastUsageDates.isEmpty()) return 0;
            Long oldestUsage = Long.MAX_VALUE;
            File oldestFile = null;
            Set<Map.Entry<File, Long>> entries = lastUsageDates.entrySet();
            synchronized (lastUsageDates) {
                for (Map.Entry<File, Long> entry : entries) {
                    Long lastValueUsage = entry.getValue();
                    if (lastValueUsage < oldestUsage) {
                        oldestUsage = lastValueUsage;
                        oldestFile = entry.getKey();
                    }
                }
            }
            if (oldestFile == null) return 0;
            long fileSize = oldestFile.length();
            if (oldestFile.delete()) {
                lastUsageDates.remove(oldestFile);
                return fileSize;
            }
            return 0;
        }
    }

    private static class CacheHelper {

        static final int timeInfoLen = 14;

        private static byte[] newByteArrayWithTime(final int second, final byte[] data) {
            byte[] time = createDueTime(second).getBytes(StandardCharsets.UTF_8);
            byte[] content = new byte[time.length + data.length];
            System.arraycopy(time, 0, content, 0, time.length);
            System.arraycopy(data, 0, content, time.length, data.length);
            return content;
        }

        /**
         * 创建过期时间
         *
         * @param second 秒
         * @return _$millis$_
         */
        private static String createDueTime(final int second) {
            return String.format(Locale.getDefault(), "_$%010d$_", System.currentTimeMillis() / 1000 + second);
        }

        private static boolean isDue(final byte[] data) {
            long millis = getDueTime(data);
            return millis != -1 && System.currentTimeMillis() > millis;
        }

        private static long getDueTime(final byte[] data) {
            if (hasTimeInfo(data)) {
                String millis = new String(copyOfRange(data, 2, 12), StandardCharsets.UTF_8);
                try {
                    return Long.parseLong(millis) * 1000;
                } catch (NumberFormatException e) {
                    return -1;
                }
            }
            return -1;
        }

        private static byte[] getDataWithoutDueTime(final byte[] data) {
            if (hasTimeInfo(data)) {
                return copyOfRange(data, timeInfoLen, data.length);
            }
            return data;
        }

        private static byte[] copyOfRange(final byte[] original, final int from, final int to) {
            int newLength = to - from;
            if (newLength < 0) throw new IllegalArgumentException(from + " > " + to);
            byte[] copy = new byte[newLength];
            System.arraycopy(original, from, copy, 0, Math.min(original.length - from, newLength));
            return copy;
        }

        private static boolean hasTimeInfo(final byte[] data) {
            return data != null
                    && data.length >= timeInfoLen
                    && data[0] == '_'
                    && data[1] == '$'
                    && data[12] == '$'
                    && data[13] == '_';
        }

        private static void writeFileFromBytes(final File file, final byte[] bytes) {
            FileChannel fc = null;
            try {
                fc = new FileOutputStream(file, false).getChannel();
                fc.write(ByteBuffer.wrap(bytes));
                fc.force(true);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                CloseUtils.closeIO(fc);
            }
        }

        private static byte[] readFile2Bytes(final File file) {
            FileChannel fc = null;
            try {
                fc = new RandomAccessFile(file, "r").getChannel();
                int size = (int) fc.size();
                MappedByteBuffer mbb = fc.map(FileChannel.MapMode.READ_ONLY, 0, size).load();
                byte[] data = new byte[size];
                mbb.get(data, 0, size);
                return data;
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } finally {
                CloseUtils.closeIO(fc);
            }
        }

        private static byte[] string2Bytes(final String string) {
            if (string == null) return null;
            return string.getBytes(StandardCharsets.UTF_8);
        }

        private static String bytes2String(final byte[] bytes) {
            if (bytes == null) return null;
            return new String(bytes, StandardCharsets.UTF_8);
        }

        private static byte[] ZSONObject2Bytes(final ZSONObject ZSONObject) {
            if (ZSONObject == null) return null;
            return ZSONObject.toString().getBytes(StandardCharsets.UTF_8);
        }

        private static ZSONObject bytes2ZSONObject(final byte[] bytes) {
            if (bytes == null) return null;
            try {
                return ZSONObject.stringToZSON(new String(bytes, StandardCharsets.UTF_8));
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        private static byte[] jsonArray2Bytes(final ZSONArray jsonArray) {
            if (jsonArray == null) return null;
            return jsonArray.toString().getBytes(StandardCharsets.UTF_8);
        }

        private static ZSONArray bytes2JSONArray(final byte[] bytes) {
            if (bytes == null) return null;
            try {
                return ZSONArray.stringToZSONArray(new String(bytes, StandardCharsets.UTF_8));
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }

        private static byte[] parcelable2Bytes(final Sequenceable parcelable) {
            if (parcelable == null) return null;
            Parcel parcel = Parcel.create();
            parcelable.marshalling(parcel);
            byte[] bytes = parcel.getBytes();
            parcel.reclaim();
            return bytes;
        }

        private static <T> T bytes2Parcelable(final byte[] bytes, final Sequenceable.Producer<T> creator) {
            if (bytes == null) return null;
            Parcel parcel = Parcel.create();
            parcel.writeBytes(bytes);
            parcel.rewindRead(0);
            T result = creator.createFromParcel(parcel);
            parcel.reclaim();
            return result;
        }

        private static byte[] serializable2Bytes(final Serializable serializable) {
            if (serializable == null) return null;
            ByteArrayOutputStream baos;
            ObjectOutputStream oos = null;
            try {
                oos = new ObjectOutputStream(baos = new ByteArrayOutputStream());
                oos.writeObject(serializable);
                return baos.toByteArray();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            } finally {
                CloseUtils.closeIO(oos);
            }
        }

        private static Object bytes2Object(final byte[] bytes) {
            if (bytes == null) return null;
            ObjectInputStream ois = null;
            try {
                ois = new ObjectInputStream(new ByteArrayInputStream(bytes));
                return ois.readObject();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            } finally {
                CloseUtils.closeIO(ois);
            }
        }

        private static byte[] bitmap2Bytes(final PixelMap bitmap) {
            if (bitmap == null) return null;
            ByteArrayOutputStream baos = new ByteArrayOutputStream();

            ImagePacker.PackingOptions options = new ImagePacker.PackingOptions();
            options.format = "image/png";
            options.quality = 100;
            ImagePacker imagePacker = ImagePacker.create();
            imagePacker.initializePacking(baos, options);
            imagePacker.addImage(bitmap);
            imagePacker.finalizePacking();

            return baos.toByteArray();
        }

        private static PixelMap bytes2Bitmap(final byte[] bytes) {
            return (bytes == null || bytes.length == 0) ? null : ImageSource.create(bytes, 0, bytes.length, null).createPixelmap(null);
        }

        private static byte[] drawable2Bytes(final Element drawable) {
            return drawable == null ? null : bitmap2Bytes(drawable2Bitmap(drawable));
        }

        private static Element bytes2Drawable(final byte[] bytes) {
            return bytes == null ? null : bitmap2Drawable(bytes2Bitmap(bytes));
        }

        private static PixelMap drawable2Bitmap(final Element drawable) {
            if (drawable instanceof PixelMapElement) {
                PixelMapElement bitmapDrawable = (PixelMapElement) drawable;
                if (bitmapDrawable.getPixelMap() != null) {
                    return bitmapDrawable.getPixelMap();
                }
            }
            PixelMap bitmap;
            PixelMap.InitializationOptions options = new PixelMap.InitializationOptions();
            if (drawable.getWidth() <= 0 || drawable.getHeight() <= 0) {
                options.size = new Size(1, 1);
            } else {
                options.size = new Size(drawable.getWidth(), drawable.getHeight());
            }
            options.pixelFormat = drawable.getAlpha() != 0 ? PixelFormat.ARGB_8888 : PixelFormat.RGB_565;
            bitmap = PixelMap.create(options);
            Canvas canvas = new Canvas(new Texture(bitmap));
            drawable.setBounds(0, 0, bitmap.getImageInfo().size.width, bitmap.getImageInfo().size.height);
            drawable.drawToCanvas(canvas);
            return bitmap;
        }

        private static Element bitmap2Drawable(final PixelMap bitmap) {
            return bitmap == null ? null : new PixelMapElement(bitmap);
        }
    }

    private static boolean isSpace(final String s) {
        if (s == null) return true;
        for (int i = 0, len = s.length(); i < len; ++i) {
            if (!Character.isWhitespace(s.charAt(i))) {
                return false;
            }
        }
        return true;
    }
}